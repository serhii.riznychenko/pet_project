package com.epam.projects.pet.controllers.admin;

import com.epam.projects.pet.models.User;
import com.epam.projects.pet.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Shows the home page for users with an administrator role,
 * loads all users who are users and shows them on the page
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.0
 */
@WebServlet(
        name = "AdminHome",
        urlPatterns = "/admin"
)
public class AdminHome extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(AdminHome.class);
    private UserService userService;

    @Override
    public void init() throws ServletException {
        userService = new UserService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<User> allUsers = userService.getAllUsersExceptAdmins();
            request.setAttribute("users", allUsers);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
            request.setAttribute("error", "database_error");
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/admin/adminHome.jsp");
        dispatcher.forward(request, response);
    }
}
