package com.epam.projects.pet.controllers.user;

import com.epam.projects.pet.models.Question;
import com.epam.projects.pet.models.TestModule;
import com.epam.projects.pet.models.TestResult;
import com.epam.projects.pet.models.User;
import com.epam.projects.pet.utils.AnswerAnalyzer;
import com.epam.projects.pet.services.QuestionService;
import com.epam.projects.pet.services.TestModuleService;
import com.epam.projects.pet.services.TestResultService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * User testing servlet. Shows testing page and assesses the results of the test
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.2
 */
@WebServlet(
        name = "Testing",
        urlPatterns = "/testing"
)
public class Testing extends HttpServlet {
    static final Logger LOG = LoggerFactory.getLogger(Testing.class);
    private TestResultService testResultService;
    private TestModuleService testModuleService;
    private QuestionService questionService;
    private AnswerAnalyzer answerAnalyzer;

    @Override
    public void init() throws ServletException {
        testResultService = new TestResultService();
        testModuleService = new TestModuleService();
        questionService = new QuestionService();
        answerAnalyzer = new AnswerAnalyzer();
    }

    /**
     * Shows a testing page
     *
     * @param request  Parameter "testResultID" it's a id of testResult that been opened by user,
     *                 this indicates that the user has updated the page with the purpose of cheating
     * @param response Testing page
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Integer testResultID = Integer.parseInt(request.getParameter("testResultID"));

        if (request.getSession().getAttribute("testResultID") != null) {
            try {
                testResultService.updateTestResult(testResultID, 0);
                request.getSession().setAttribute("testResultID", null);
            } catch (SQLException e) {
                LOG.error(e.getMessage());
            }
            response.sendRedirect("/quiz-engine/home/tests");
            return;
        }

        if (request.getParameter("testResultID") == null) {
            LOG.error("testResultID parameter is empty");
            response.sendRedirect("/quiz-engine/home/tests");
            return;
        } else {
            testResultID = Integer.valueOf(request.getParameter("testResultID"));
        }

        try {
            TestResult testResult = testResultService.getTestResult(testResultID);
            User user = (User) request.getSession().getAttribute("loggedUser");
            if (testResult.getUserID() != user.getUserID()) {
                LOG.warn("User does not have access to the test");
                response.sendRedirect("/quiz-engine/home/tests");
                return;
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }

        request.getSession().setAttribute("testResultID", testResultID);
        try {
            TestResult testResult = testResultService.getTestResult(testResultID);
            TestModule testModule = testModuleService.getTest(testResult.getTestID());
            testModule.setQuestions(questionService.loadListOfAnswersIntoListOfQuestions(testResult.getTestID()));
            request.setAttribute("testModule", testModule);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/user/userTesting.jsp");
        dispatcher.forward(request, response);
    }

    /**
     * Analyzes the test result
     *
     * @param request  Parameter "testResultID" it's a id of testResult that been opened by user,
     *                 array of parameters "answer", which contains id's of answers, which user chose
     * @param response Redirect to user home page
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Integer testResultID = (int) request.getSession().getAttribute("testResultID");

        if (testResultID == null) {
            LOG.error("testResultID parameter is empty");
            response.sendRedirect("/quiz-engine/home/tests");
            return;
        }

        try {
            TestResult testResult = testResultService.getTestResult(testResultID);
            ArrayList<Question> questions = questionService.loadListOfAnswersIntoListOfQuestions(testResult.getTestID());
            String[] userAnswers = request.getParameterValues("answer");
            int points = answerAnalyzer.getPercentOfCorrectAnswers(questions, userAnswers);
            testResultService.updateTestResult(testResultID, points);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }

        request.setAttribute("testResultID", null);
        response.sendRedirect("/quiz-engine/home");
    }
}
