package com.epam.projects.pet.services;

import com.epam.projects.pet.dao.UserDao;
import com.epam.projects.pet.models.User;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class to operate with users
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.1
 */
public class UserService {

    public List<User> getAllUsersExceptAdmins() throws SQLException {
        UserDao userDao = new UserDao();
        try {
            return userDao.getAll().stream().filter(user ->
                    !user.getRole().equals(User.Role.ADMIN)).collect(Collectors.toList());
        } finally {
            userDao.returnConnection();
        }
    }

    public void changeUserBanStatusById(int id) throws SQLException {
        UserDao userDao = new UserDao();
        try {
            User user = userDao.getEntityById(id);
            if (user.getIsBanned()) {
                userDao.banUserOp(id, false);
            } else userDao.banUserOp(id, true);
        } finally {
            userDao.returnConnection();
        }
    }

    public void updateUserData(String login, String email, String realName, int age, User currentUser) throws SQLException {
        if (login != null) currentUser.setLogin(login);
        if (email != null) currentUser.setEmail(email);
        if (realName != null) currentUser.setRealName(realName);
        if (age != 0) currentUser.setAge(age);
        UserDao userDao = new UserDao();
        try {
            userDao.update(currentUser);
        } finally {
            userDao.returnConnection();
        }
    }

    public boolean deleteUser(User user) throws SQLException {
        UserDao userDao = new UserDao();
        try {
            return userDao.delete(user.getUserID());
        } finally {
            userDao.returnConnection();
        }
    }

    public boolean makeAdmin(int userID, String login) throws SQLException {
        UserDao userDao = new UserDao();
        try {
            if (userID <= 0 || login == null || login.isEmpty()) return false;
            User user = userDao.getEntityById(userID);

            if (!(user == null) && user.getLogin().equals(login)) {
                user.setRole(User.Role.ADMIN);
                userDao.update(user);
                return true;
            } else {
                return false;
            }
        } finally {
            userDao.returnConnection();
        }
    }
}
