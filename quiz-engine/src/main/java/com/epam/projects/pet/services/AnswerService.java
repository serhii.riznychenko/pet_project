package com.epam.projects.pet.services;

import com.epam.projects.pet.dao.AnswerDao;
import com.epam.projects.pet.models.Answer;
import com.epam.projects.pet.models.Question;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to operate with test answers
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.1
 */
public class AnswerService {

    public Answer createAnswer(String answer, boolean isCorrect, int questionID) throws SQLException {
        AnswerDao answerDao = new AnswerDao();
        try {
            Answer newAnswer = new Answer();
            newAnswer.setAnswer(answer);
            newAnswer.setCorrect(isCorrect);
            newAnswer.setQuestionID(questionID);
            answerDao.create(newAnswer);
            return newAnswer;
        } finally {
            answerDao.returnConnection();
        }
    }

    public Answer getAnswer(int answerID) throws SQLException {
        AnswerDao answerDao = new AnswerDao();
        try {
            return answerDao.getEntityById(answerID);
        } finally {
            answerDao.returnConnection();
        }
    }

    public List<Answer> getAnswersByQuestionID(int questionID) throws SQLException {
        AnswerDao answerDao = new AnswerDao();
        try {
            return answerDao.getAnswersByQuestionId(questionID);
        } finally {
            answerDao.returnConnection();
        }
    }

    public Answer updateAnswer(int answerID, String answer, boolean isCorrect, int questionID) throws SQLException {
        AnswerDao answerDao = new AnswerDao();
        try {
            Answer newAnswer = new Answer();
            newAnswer.setAnswerID(answerID);
            newAnswer.setAnswer(answer);
            newAnswer.setCorrect(isCorrect);
            newAnswer.setQuestionID(questionID);
            answerDao.update(newAnswer);
            return newAnswer;
        } finally {
            answerDao.returnConnection();
        }
    }

    public void deleteAnswerByID(int answerID) throws SQLException {
        AnswerDao answerDao = new AnswerDao();
        try {
            answerDao.delete(answerID);
        } finally {
            answerDao.returnConnection();
        }
    }
}
