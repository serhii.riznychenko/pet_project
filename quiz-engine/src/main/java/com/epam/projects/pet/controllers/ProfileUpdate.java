package com.epam.projects.pet.controllers;

import com.epam.projects.pet.models.User;
import com.epam.projects.pet.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(
        name = "ProfileUpdate",
        urlPatterns = "/profile/update"
)
public class ProfileUpdate extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(ProfileUpdate.class);
    UserService userService = new UserService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/profileUpdate.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String email = request.getParameter("email");
        // String password = request.getParameter("password");
        String realName = request.getParameter("realName");
        int age = Integer.parseInt(request.getParameter("age"));
        User user = (User) request.getSession().getAttribute("loggedUser");
        try {
            userService.updateUserData(login, email, realName, age, user);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
            request.setAttribute("error", "database_error");
            doGet(request, response);
        }

        response.sendRedirect("/quiz-engine/profile");
    }
}
