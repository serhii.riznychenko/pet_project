package com.epam.projects.pet.services;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * Class to operate with user locale
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.1
 */
public class LocaleService {

    public void setLocale(HttpServletRequest request) {
        if (request.getLocale() == null) {
            request.getSession().setAttribute("LOCALE", new Locale("en", "US"));
        } else request.getSession().setAttribute("LOCALE", request.getLocale());
    }
}
