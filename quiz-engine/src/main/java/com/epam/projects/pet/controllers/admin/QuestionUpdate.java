package com.epam.projects.pet.controllers.admin;

import com.epam.projects.pet.models.Answer;
import com.epam.projects.pet.models.Question;
import com.epam.projects.pet.services.AnswerService;
import com.epam.projects.pet.services.QuestionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Question update servlet
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.0
 */
@WebServlet(
        name = "QuestionUpdate",
        urlPatterns = "/admin/tests/questions/update"
)
public class QuestionUpdate extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(QuestionUpdate.class);
    private QuestionService questionService;
    private AnswerService answerService;

    @Override
    public void init() throws ServletException {
        questionService = new QuestionService();
        answerService = new AnswerService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int questionID = Integer.parseInt(request.getParameter("questionID"));
        try {
            Question question = questionService.getQuestion(questionID);
            request.setAttribute("question", question);
            List<Answer> answers = answerService.getAnswersByQuestionID(questionID);
            request.setAttribute("answers", answers);

        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/admin/questionUpdate.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int questionID = Integer.parseInt(request.getParameter("questionID"));
        String question = request.getParameter("question");
        int testID = Integer.parseInt(request.getParameter("testID"));

        try {
            questionService.updateQuestion(questionID, question, testID);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
            response.sendRedirect("/error-pages/nothingFound.jsp");
        } finally {
            response.sendRedirect("/quiz-engine/admin/tests/update?testID=" + testID);
        }
    }
}
