package com.epam.projects.pet.controllers.admin;

import com.epam.projects.pet.controllers.user.UserTests;
import com.epam.projects.pet.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Changes the ban status of a user
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.0
 */
@WebServlet(
        name = "UserBanStatusChange",
        urlPatterns = "/admin/ban"
)
public class UserBanStatusChange extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(UserTests.class);
    UserService userService;

    @Override
    public void init() throws ServletException {
        userService = new UserService();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int userID = Integer.parseInt(request.getParameter("userID"));
        try {
            userService.changeUserBanStatusById(userID);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            response.sendRedirect("/error-pages/nothingFound.jsp");
        }
        response.sendRedirect("/quiz-engine/admin");
    }
}
