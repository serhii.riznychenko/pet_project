package com.epam.projects.pet.controllers;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;


@WebServlet(
        name = "LocaleChanging",
        urlPatterns = "/locale-change"
)
public class LocaleChanging extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String uri = request.getRequestURI();
        if (request.getParameter("locale") != null) {
            String localeConf = request.getParameter("locale");
            Locale locale = new Locale(localeConf);
            request.getSession().setAttribute("LOCALE", locale);
        } else response.sendError(400);
        response.sendRedirect("/quiz-engine");
    }
}