package com.epam.projects.pet.controllers.user;

import com.epam.projects.pet.models.TestResult;
import com.epam.projects.pet.models.User;
import com.epam.projects.pet.services.QuestionService;
import com.epam.projects.pet.services.TestResultService;
import com.epam.projects.pet.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Test list servlet
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.0
 */
@WebServlet(
        name = "UserTests",
        urlPatterns = "/home/tests"
)
public class UserTests extends HttpServlet {

    private static final Logger LOG = LoggerFactory.getLogger(UserTests.class);
    private UserService userService;
    private TestResultService testResultService;
    private QuestionService questionService;

    @Override
    public void init() throws ServletException {
        userService = new UserService();
        testResultService = new TestResultService();
        questionService = new QuestionService();
    }

    /**
     * Loads a set of user test results, with full load test set inside,
     * by default, sort tests by name
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        User currentUser = (User) request.getSession().getAttribute("loggedUser");
        String sortType = (String) request.getSession().getAttribute("sortType");
        String topic = request.getParameter("topic");

        try {
            Map<String, ArrayList<TestResult>> testsResultMap =
                    testResultService.loadSetOfNonEmptyTestsIntoTestResultMap(currentUser.getUserID(), false);

            for (Map.Entry<String, ArrayList<TestResult>> entry : testsResultMap.entrySet()) {
                for (TestResult testResult : entry.getValue()) {
                    testResult.getTestModule().setQuestions(
                            questionService.loadListOfAnswersIntoListOfQuestions(testResult.getTestID()));
                }
            }
            if (sortType != null) {
                testResultService.sortTestResults(testsResultMap, sortType);
            } else {
                testResultService.sortTestResults(testsResultMap, "name");
            }
            if (topic == null) {
                request.setAttribute("testsResultMap", testsResultMap);
            } else {
                request.setAttribute("testsResultMap", testResultService.topicChoice(testsResultMap, topic));
            }

            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/user/userTests.jsp");
            dispatcher.forward(request, response);


        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }
}
