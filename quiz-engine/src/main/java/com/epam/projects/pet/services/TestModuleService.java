package com.epam.projects.pet.services;

import com.epam.projects.pet.dao.AnswerDao;
import com.epam.projects.pet.dao.QuestionDao;
import com.epam.projects.pet.dao.TestModuleDao;
import com.epam.projects.pet.models.Answer;
import com.epam.projects.pet.models.Question;
import com.epam.projects.pet.models.TestModule;

import java.sql.SQLException;
import java.util.List;

/**
 * Class to operate with test modules
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.1
 */
public class TestModuleService {

    public void createTest(String testName, int testHardLevel, String testTopic,
                           int timeToSolveTest) throws SQLException {
        TestModuleDao testModuleDao = new TestModuleDao();
        try {
            TestModule newTest = new TestModule();
            newTest.setName(testName);
            newTest.setHardLevel(testHardLevel);
            newTest.setTopic(testTopic);
            newTest.setTimeToSolve(timeToSolveTest);
            testModuleDao.create(newTest);
        } finally {
            testModuleDao.returnConnection();
        }
    }

    public TestModule getTest(int testID) throws SQLException {
        TestModuleDao testModuleDao = new TestModuleDao();
        try {
            return testModuleDao.getEntityById(testID);
        } finally {
            testModuleDao.returnConnection();
        }
    }

    public List<TestModule> getAllTests() throws SQLException {
        TestModuleDao testModuleDao = new TestModuleDao();
        try {
            return testModuleDao.getAll();
        } finally {
            testModuleDao.returnConnection();
        }
    }

    public void updateTest(int testID, String testName, int testHardLevel, String testTopic,
                           int timeToSolveTest) throws SQLException {
        TestModuleDao testModuleDao = new TestModuleDao();
        try {
            TestModule updatedTest = new TestModule();
            updatedTest.setTestID(testID);
            updatedTest.setName(testName);
            updatedTest.setHardLevel(testHardLevel);
            updatedTest.setTopic(testTopic);
            updatedTest.setTimeToSolve(timeToSolveTest);
            testModuleDao.update(updatedTest);
        } finally {
            testModuleDao.returnConnection();
        }
    }

    public void deleteTest(int testID) throws SQLException {
        TestModuleDao testModuleDao = new TestModuleDao();
        QuestionDao questionDao = new QuestionDao();
        AnswerDao answerDao = new AnswerDao();
        QuestionService questionService = new QuestionService();
        try {
            List<Question> questions = questionService.loadListOfAnswersIntoListOfQuestions(testID);
            for (Question question : questions) {
                for (Answer answer : question.getAnswers()) {
                    answerDao.delete(answer.getAnswerID());
                }
                questionDao.delete(question.getQuestionID());
            }
            testModuleDao.delete(testID);

        } finally {
            testModuleDao.returnConnection();
            questionDao.returnConnection();
            answerDao.returnConnection();
        }
    }

    /**
     * Loads a list of questions to the each test
     *
     * @param testModules Test list
     * @return A list of tests with loaded questions in them
     * @throws SQLException
     */
    public List<TestModule> loadSetOfQuestionsToTestModule(List<TestModule> testModules) throws SQLException {
        QuestionDao questionDao = new QuestionDao();
        try {
            for (TestModule test : testModules) {
                test.setQuestions(questionDao.getQuestionsByTestId(test.getTestID()));
            }
            return testModules;
        } finally {
            questionDao.returnConnection();
        }
    }
}
