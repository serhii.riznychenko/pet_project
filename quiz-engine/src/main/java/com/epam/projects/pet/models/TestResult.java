package com.epam.projects.pet.models;

import java.io.Serializable;

public class TestResult implements Serializable {
    private static final long serialVersionUID = 7454341023908685827L;

    private int resultID;
    private int userID;
    private int testID;
    private boolean isDone;
    private int points;
    private TestModule testModule;

    public TestResult() {}

    public int getResultID() {
        return resultID;
    }

    public void setResultID(int resultID) {
        this.resultID = resultID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getTestID() {
        return testID;
    }

    public void setTestID(int testID) {
        this.testID = testID;
    }

    public boolean getIsDone() {
        return isDone;
    }

    public void setIsDone(boolean done) {
        isDone = done;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public TestModule getTestModule() {
        return testModule;
    }

    public void setTestModule(TestModule testModule) {
        this.testModule = testModule;
    }

    @Override
    public int hashCode() {
        int result = resultID;
        result = 42 * result + (testModule != null ? testModule.hashCode() : 0);
        result += 42 * userID;
        result += 42 * testID;
        return result;
    }

    @Override
    public String toString() {
        return "Test result {" +
                "result id='" + resultID + '\'' +
                ", user id='" + userID + '\'' +
                ", test id='" + testID + '\'' +
                ", is done='" + isDone + '\'' +
                ", points='" + points + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestResult testResult = (TestResult) o;
        if (userID != 0 ? userID != testResult.userID : testResult.userID != 0) return false;
        if (testID != 0 ? testID != testResult.testID : testResult.testID != 0) return false;
        return points >= 0 ? points == (testResult.points) : false;
    }
}
