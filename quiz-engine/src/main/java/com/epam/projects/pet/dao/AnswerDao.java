package com.epam.projects.pet.dao;

import com.epam.projects.pet.models.Answer;
import com.epam.projects.pet.models.Question;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AnswerDao extends AbstractDao<Answer, Integer> {
    public AnswerDao() {
    }

    @Override
    public List<Answer> getAll() throws SQLException {
        ArrayList<Answer> answers = new ArrayList<>();
        String SELECT_ALL_ANSWERS = "select * from answers";
        PreparedStatement ps = getPrepareStatement(SELECT_ALL_ANSWERS);
        try {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Answer answer = new Answer();
                answer.setAnswerID(rs.getInt(1));
                answer.setAnswer(rs.getString(2));
                answer.setCorrect(rs.getBoolean(3));
                answer.setQuestionID(rs.getInt(4));
                answers.add(answer);
            }
        } finally {
            closePrepareStatement(ps);
        }
        return answers;
    }

    @Override
    public Answer update(Answer entity) throws SQLException {
        String UPDATE_ANSWER = "update answers set answer = ?, is_correct = ?, questionID = ? where answerID = ?";
        PreparedStatement ps = getPrepareStatement(UPDATE_ANSWER);
        try {
            ps.setString(1, entity.getAnswer());
            ps.setBoolean(2, entity.isCorrect());
            ps.setInt(3, entity.getQuestionID());
            ps.setInt(4, entity.getAnswerID());
            ps.executeUpdate();
        } finally {
            closePrepareStatement(ps);
        }
        return entity;
    }

    @Override
    public Answer getEntityById(Integer id) throws SQLException {
        String SELECT_ANSWER = "select * from answers where answerID = ?";
        PreparedStatement ps = getPrepareStatement(SELECT_ANSWER);
        Answer answer = new Answer();
        try {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                answer.setAnswerID(rs.getInt(1));
                answer.setAnswer(rs.getString(2));
                answer.setCorrect(rs.getBoolean(3));
                answer.setQuestionID(rs.getInt(4));
            }
        } finally {
            closePrepareStatement(ps);
        }
        return answer;
    }

    @Override
    public boolean delete(Integer id) throws SQLException {
        String DELETE_ANSWER = "delete from answers where answerID = ?";
        PreparedStatement ps = getPrepareStatement(DELETE_ANSWER);
        try {
            ps.setInt(1, id);
            ps.executeUpdate();
        } finally {
            closePrepareStatement(ps);
        }
        return true;
    }

    @Override
    public boolean create(Answer entity) throws SQLException {
        String CREATE_ANSWER = "insert into answers (answer, is_correct, questionID) values (?, ?, ?)";
        PreparedStatement ps = getPrepareStatement(CREATE_ANSWER);
        try {
            ps.setString(1, entity.getAnswer());
            ps.setBoolean(2, entity.isCorrect());
            ps.setInt(3, entity.getQuestionID());
            ps.executeUpdate();
        } finally {
            closePrepareStatement(ps);
        }
        return true;
    }

    public ArrayList<Answer> getAnswersByQuestionId(Integer questionID) throws SQLException {
        String SELECT_ANSWERS_BY_QUESTIONID = "select * from answers where questionID = ?";
        PreparedStatement ps = getPrepareStatement(SELECT_ANSWERS_BY_QUESTIONID);
        ArrayList<Answer> answers = new ArrayList<>();
        try {
            ps.setInt(1, questionID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Answer answer = new Answer();
                answer.setAnswerID(rs.getInt(1));
                answer.setAnswer(rs.getString(2));
                answer.setCorrect(rs.getBoolean(3));
                answer.setQuestionID(questionID);
                answers.add(answer);
            }
        } finally {
            closePrepareStatement(ps);
        }
        return answers;
    }


}
