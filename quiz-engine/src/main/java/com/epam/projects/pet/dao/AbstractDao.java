package com.epam.projects.pet.dao;

import com.epam.projects.pet.utils.ConnectionPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public abstract class AbstractDao<E, K> {
    private static Logger LOG = LoggerFactory.getLogger(AbstractDao.class);

    protected Connection connection;
    private ConnectionPool connectionPool;

    protected AbstractDao() {
        connectionPool = ConnectionPool.getInstance();
        connection = connectionPool.getConnection();
    }

    public abstract List<E> getAll() throws SQLException;

    public abstract E update(E entity) throws SQLException;

    public abstract E getEntityById(K id) throws SQLException;

    public abstract boolean delete(K id) throws SQLException;

    public abstract boolean create(E entity) throws SQLException;

    public void returnConnection() throws SQLException {
        connection.close();
    }

    protected PreparedStatement getPrepareStatement(String sql) {
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
        } catch (SQLException e) {
            LOG.info(e.getMessage());
            LOG.warn(e.getMessage());
        }
        return ps;
    }

    protected void closePrepareStatement(PreparedStatement ps) {
        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException e) {
                LOG.info(e.getMessage());
                LOG.warn(e.getMessage());
            }
        }
    }
}