package com.epam.projects.pet.controllers.admin;

import com.epam.projects.pet.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(
        name = "CreateNewAdmin",
        urlPatterns = "/admin/create"
)
public class CreateNewAdmin extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(CreateNewAdmin.class);
    private UserService userService;

    @Override
    public void init() throws ServletException {
        userService = new UserService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/admin/createNewAdmin.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer userID = Integer.valueOf(request.getParameter("userID"));
        String login = request.getParameter("login");
        try {
            if (userService.makeAdmin(userID, login) == false) {
                LOG.info("User id and login are not same!");
                request.setAttribute("error", "id_and_login_are_not_same");
                doGet(request, response);
                return;
            } else response.sendRedirect("/quiz-engine/admin");

        } catch (SQLException e) {
            LOG.error(e.getMessage());
            request.setAttribute("error", "database_error");
            doGet(request, response);
            return;
        }
    }
}
