package com.epam.projects.pet.utils;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validates the data
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.4
 */
public class Validator {

    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private static final String LOGIN_PATTERN = "^[\\w0-9]{3,20}$";

    private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{5,20})";

    private static final String NAME_PATTERN = "^\\w+$";



    public boolean isLoginValid(String login) {
        if (login == null || login.isEmpty()) {
            return false;
        } else {
            Pattern pattern = Pattern.compile(LOGIN_PATTERN);
            return pattern.matcher(login).matches();
        }
    }

    public boolean isPasswordValid(String password) {
        if (password == null || password.isEmpty()) {
            return false;
        } else {
            Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
            return pattern.matcher(password).matches();
        }
    }

    public boolean isAgeValid(String age) {
        if (age == null || age.isEmpty()) {
            return false;
        } else {
            int intAge = Integer.parseInt(age);
            if (intAge <= 5 || intAge >= 95) {
                return false;
            } else {
                return true;
            }
        }
    }

    public boolean isEmailValid(String email) {
        if (email == null || email.isEmpty()) {
            return false;
        } else {
            Pattern pattern = Pattern.compile(EMAIL_PATTERN);
            return pattern.matcher(email).matches();
        }
    }

    public boolean isNameValid(String name) {
        if (name == null || name.isEmpty()) {
            return false;
        } else {
            Pattern pattern = Pattern.compile(NAME_PATTERN);
            return pattern.matcher(name).matches();
        }
    }

    public boolean isDocmentValid(String document) {
        return (document == null || document.length() < 5) ? false : true;
    }
}
