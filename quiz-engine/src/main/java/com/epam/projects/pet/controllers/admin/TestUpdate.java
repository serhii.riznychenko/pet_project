package com.epam.projects.pet.controllers.admin;

import com.epam.projects.pet.models.Question;
import com.epam.projects.pet.models.TestModule;
import com.epam.projects.pet.services.QuestionService;
import com.epam.projects.pet.services.TestModuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Test update servlet
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.0
 */
@WebServlet(
        name = "TestUpdate",
        urlPatterns = "/admin/tests/update"
)
public class TestUpdate extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(TestUpdate.class);
    private QuestionService questionService;
    private TestModuleService testModuleService;

    @Override
    public void init() throws ServletException {
        questionService = new QuestionService();
        testModuleService = new TestModuleService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int testID = Integer.parseInt(request.getParameter("testID"));

        try {
            TestModule testModule = testModuleService.getTest(testID);
            request.setAttribute("test", testModule);
            List<Question> questions = questionService.loadListOfAnswersIntoListOfQuestions(testID);
            request.setAttribute("questions", questions);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
            response.sendRedirect("/error-pages/nothingFound.jsp");
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/admin/testUpdate.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int testID = Integer.parseInt(request.getParameter("testID"));
        String testName = request.getParameter("name");
        int testHardLevel = Integer.parseInt(request.getParameter("hardLevel"));
        String testTopic = request.getParameter("topic");
        int timeToSolveTest = Integer.parseInt(request.getParameter("timeToSolve"));
        try {
            testModuleService.updateTest(testID, testName, testHardLevel, testTopic, timeToSolveTest);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
       } finally {
            response.sendRedirect("/quiz-engine/admin/tests/update?testID=" + testID);
        }
    }
}