package com.epam.projects.pet.utils;

import com.epam.projects.pet.controllers.user.Testing;
import com.epam.projects.pet.models.Answer;
import com.epam.projects.pet.models.Question;

import java.util.List;

/**
 * Assesses the results of the test
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.1
 */
public class AnswerAnalyzer {

    private int getAmountOfCorrectAnswers(List<Question> questions) {
        int AmountOfCorrectAnswers = 0;
        for (Question question : questions) {
            AmountOfCorrectAnswers += question.getAnswers().stream().filter(p -> (p.isCorrect())).count();
        }
        return AmountOfCorrectAnswers;
    }

    /**
     * Passes through all the answers on each question,
     * if at least one answer to the question is incorrect,
     * PassAnswer marking false, if passing the question PassAnswer still true,
     * points incrementing
     *
     * @param questions   List of test questions
     * @param userAnswers Array of user answers
     * @return Percentage of correct answers to questions
     */
    public int getPercentOfCorrectAnswers(List<Question> questions, String[] userAnswers) {
        double oneHundredPercent = 100;
        double points = 0;
        if (userAnswers != null && userAnswers.length > 0) {
            int amountOfCorrectAnswers = getAmountOfCorrectAnswers(questions);
            for (Question question : questions) {
                boolean passAnswer = true;
                for (Answer answer : question.getAnswers()) {
                    for (String userAnswer : userAnswers) {
                        if (answer.getAnswerID() == Integer.parseInt(userAnswer)) {
                            if (!answer.isCorrect()) passAnswer = false;
                            else break;
                        }
                    }
                }
                if (passAnswer) points++;
            }
            if (amountOfCorrectAnswers > 0) {
                points *= (oneHundredPercent / amountOfCorrectAnswers);
            }
        }
        return (int) Math.floor(points);
    }
}
