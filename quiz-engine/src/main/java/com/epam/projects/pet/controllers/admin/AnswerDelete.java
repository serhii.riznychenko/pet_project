package com.epam.projects.pet.controllers.admin;

import com.epam.projects.pet.services.AnswerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Answer delete servlet
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.0
 */
@WebServlet(
        name = "AnswerDelete",
        urlPatterns = "/admin/tests/questions/answers/delete"
)
public class AnswerDelete extends HttpServlet {
    private AnswerService answerService;

    @Override
    public void init() throws ServletException {
        answerService = new AnswerService();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int answerID = Integer.parseInt(request.getParameter("answerID"));
        int questionID = Integer.parseInt(request.getParameter("questionID"));
        try {
            answerService.deleteAnswerByID(answerID);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        response.sendRedirect("/quiz-engine/admin/tests/questions/update?questionID=" + questionID);
    }
}
