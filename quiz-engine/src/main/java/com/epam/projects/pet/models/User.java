package com.epam.projects.pet.models;

import java.io.Serializable;

public class User implements Serializable {
    private static final long serialVersionUID = 2542279845534863621L;

    public enum Role {USER, ADMIN}

    private int userID;
    private String login;
    private String email;
    private char[] password = new char[60];
    private String realName;
    private boolean isBanned;
    private int age = 0;
    private Role role = Role.USER;

    public User() {}

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public char[] getPassword() {
        return password;
    }

    public void setPassword(char[] password) {
        this.password = password;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public boolean getIsBanned() {
        return isBanned;
    }

    public void setIsBanned(boolean isBanned) {
        this.isBanned = isBanned;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        if (login != null ? !login.equals(user.login) : user.login != null) return false;
        return password != null ? password.equals(user.password) : user.password == null;
    }

    @Override
    public int hashCode() {
        int result = userID;
        result = 42 * result + (login != null ? login.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User {" +
                "login='" + login + '\'' +
                ", email='" + email + '\'' +
                ", name='" + realName + '\'' +
                ", is banned='" + isBanned + '\'' +
                ", age='" + age + '\'' +
                ", role='" + role + '\'' +
                '}';
    }

}
