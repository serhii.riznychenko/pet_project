package com.epam.projects.pet.controllers.user;

import com.epam.projects.pet.models.TestResult;
import com.epam.projects.pet.services.TestModuleService;
import com.epam.projects.pet.services.TestResultService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Sorts tests
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.0
 */
@WebServlet(
        name = "TestsSortServlet",
        urlPatterns = "/home/tests/sorting"
)
public class TestsSortServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = request.getParameter("url");
        String sortType = request.getParameter("sort");
        request.getSession().setAttribute("sortType", sortType);
        response.sendRedirect(url);
    }
}
