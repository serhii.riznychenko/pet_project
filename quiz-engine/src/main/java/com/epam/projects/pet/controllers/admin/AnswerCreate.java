package com.epam.projects.pet.controllers.admin;

import com.epam.projects.pet.services.AnswerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Answer create servlet
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.0
 */
@WebServlet(
        name = "AnswerCreate",
        urlPatterns = "/admin/tests/questions/answers/create"
)
public class AnswerCreate extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(AnswerCreate.class);
    AnswerService answerService;

    @Override
    public void init() throws ServletException {
        answerService = new AnswerService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/admin/answerCreate.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String answer = request.getParameter("answer");
        boolean isCorrect = Boolean.parseBoolean(request.getParameter("isCorrect"));
        int questionID = Integer.parseInt(request.getParameter("questionID"));

        try {
            answerService.createAnswer(answer, isCorrect, questionID);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        } finally {
            response.sendRedirect("/quiz-engine/admin/tests/questions/update?questionID=" + questionID);
        }
    }
}
