package com.epam.projects.pet.controllers;

import com.epam.projects.pet.controllers.user.UserHome;
import com.epam.projects.pet.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(
        name = "Profile",
        urlPatterns = "/profile"
)
public class Profile extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(UserHome.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/profile.jsp");
        dispatcher.forward(request, response);
    }
}
