package com.epam.projects.pet.listeners;

import com.epam.projects.pet.utils.ConnectionPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ConnectionPoolListener implements ServletContextListener {
    private static Logger LOG = LoggerFactory.getLogger(ConnectionPool.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        LOG.info("Closing connection pool");
        ConnectionPool.getInstance().shutdown();
    }
}