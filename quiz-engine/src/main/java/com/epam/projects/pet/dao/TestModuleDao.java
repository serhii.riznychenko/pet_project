package com.epam.projects.pet.dao;

import com.epam.projects.pet.models.TestModule;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TestModuleDao extends AbstractDao<TestModule, Integer> {

    public TestModuleDao() {
    }

    @Override
    public List<TestModule> getAll() throws SQLException {
        ArrayList<TestModule> userTests = new ArrayList<>();
        String SELECT_ALL_TESTS = "select * from tests";
        PreparedStatement ps = getPrepareStatement(SELECT_ALL_TESTS);
        try {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TestModule testModule = new TestModule();
                testModule.setTestID(rs.getInt(1));
                testModule.setName(rs.getString(2));
                testModule.setHardLevel(rs.getInt(3));
                testModule.setTopic(rs.getString(4));
                testModule.setTimeToSolve(rs.getInt(5));
                userTests.add(testModule);
            }
        } finally {
            closePrepareStatement(ps);
        }
        return userTests;
    }

    @Override
    public TestModule update(TestModule entity) throws SQLException {
        String UPDATE_TEST = "update tests set name = ?, hard_level = ?, topic = ?," +
                " time_to_solve = ? where testID = ?";
        PreparedStatement ps = getPrepareStatement(UPDATE_TEST);
        try {
            ps.setString(1, entity.getName());
            ps.setInt(2, entity.getHardLevel());
            ps.setString(3, entity.getTopic());
            ps.setInt(4, entity.getTimeToSolve());
            ps.setInt(5, entity.getTestID());
            ps.executeUpdate();
        } finally {
            closePrepareStatement(ps);
        }
        return entity;
    }

    @Override
    public TestModule getEntityById(Integer id) throws SQLException {
        String SELECT_TEST = "select * from tests where testID = ?";
        PreparedStatement ps = getPrepareStatement(SELECT_TEST);
        TestModule testModule = new TestModule();
        try {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                testModule.setTestID(id);
                testModule.setName(rs.getString(2));
                testModule.setHardLevel(rs.getInt(3));
                testModule.setTopic(rs.getString(4));
                testModule.setTimeToSolve(rs.getInt(5));
            }
        } finally {
            closePrepareStatement(ps);
        }
        return testModule;
    }

    @Override
    public boolean delete(Integer id) throws SQLException {
        String DELETE_TEST = "delete from tests where testID = ?";
        PreparedStatement ps = getPrepareStatement(DELETE_TEST);
        try {
            ps.setInt(1, id);
            ps.executeUpdate();
        } finally {
            closePrepareStatement(ps);
        }
        return true;
    }

    @Override
    public boolean create(TestModule entity) throws SQLException {
        String CREATE_TEST = "insert into tests (name, hard_level, topic, time_to_solve) values (?, ?, ?, ?)";
        PreparedStatement ps = getPrepareStatement(CREATE_TEST);
        try {
            ps.setString(1, entity.getName());
            ps.setInt(2, entity.getHardLevel());
            ps.setString(3, entity.getTopic());
            ps.setInt(4, entity.getTimeToSolve());
            ps.executeUpdate();
        } finally {
            closePrepareStatement(ps);
        }
        return true;
    }
}
