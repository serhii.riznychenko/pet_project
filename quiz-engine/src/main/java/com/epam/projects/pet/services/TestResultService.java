package com.epam.projects.pet.services;

import com.epam.projects.pet.dao.TestModuleDao;
import com.epam.projects.pet.dao.TestResultDao;
import com.epam.projects.pet.models.TestResult;

import java.sql.SQLException;
import java.util.*;

/**
 * Class to operate with test results
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.1
 */
public class TestResultService {

    public void createTestResult(int userID, int testID) throws SQLException {
        TestResultDao resultDao = new TestResultDao();
        try {
            TestResult result = new TestResult();
            result.setUserID(userID);
            result.setTestID(testID);
            resultDao.create(result);
        } finally {
            resultDao.returnConnection();
        }
    }

    public TestResult getTestResult(int resultID) throws SQLException {
        TestResultDao resultDao = new TestResultDao();
        try {
            return resultDao.getEntityById(resultID);
        } finally {
            resultDao.returnConnection();
        }
    }

    public boolean updateTestResult(int resultID, int points) throws SQLException {
        TestResult result = getTestResult(resultID);
        TestResultDao resultDao = new TestResultDao();
        try {
            result.setPoints(points);
            result.setIsDone(true);
            resultDao.update(result);
        } finally {
            resultDao.returnConnection();
        }
        return true;
    }

    public List<TestResult> getAllTestResults() throws SQLException {
        TestResultDao resultDao = new TestResultDao();
        try {
            return resultDao.getAll();
        } finally {
            resultDao.returnConnection();
        }
    }

    public void deleteTestResult(int resultID) throws SQLException {
        TestResultDao resultDao = new TestResultDao();
        try {
            resultDao.delete(resultID);
        } finally {
            resultDao.returnConnection();
        }
    }

    public ArrayList<TestResult> loadSetOfNonEmptyTestsIntoTestResultList(int userID) throws SQLException {
        TestResultDao resultDao = new TestResultDao();
        TestModuleDao testModuleDao = new TestModuleDao();
        try {
            ArrayList<TestResult> results = resultDao.getResultsByUserId(userID);
            for (TestResult result : results) {
                if (testModuleDao.getEntityById(result.getTestID()) != null) {
                    result.setTestModule(testModuleDao.getEntityById(result.getTestID()));
                }
            }
            return results;
        } finally {
            resultDao.returnConnection();
            testModuleDao.returnConnection();
        }
    }

    public Map<String, ArrayList<TestResult>> loadSetOfNonEmptyTestsIntoTestResultMap(int userID, boolean isDone) throws SQLException {
        TestResultDao resultDao = new TestResultDao();
        TestModuleDao testModuleDao = new TestModuleDao();
        try {
            Map<String, ArrayList<TestResult>> multiMap = new HashMap<String, ArrayList<TestResult>>();
            ArrayList<TestResult> results = resultDao.getResultsByUserId(userID);
            for (TestResult result : results) {
                if (result.getIsDone() == isDone) {
                    if (testModuleDao.getEntityById(result.getTestID()) != null) {
                        result.setTestModule(testModuleDao.getEntityById(result.getTestID()));

                        if (!multiMap.containsKey(result.getTestModule().getTopic())) {
                            ArrayList<TestResult> newTopicList = new ArrayList<>();
                            newTopicList.add(result);
                            multiMap.put(result.getTestModule().getTopic(), newTopicList);
                        } else {
                            multiMap.get(result.getTestModule().getTopic()).add(result);
                        }

                    }
                }
            }
            return multiMap;
        } finally {
            resultDao.returnConnection();
            testModuleDao.returnConnection();
        }
    }

    public Map<String, ArrayList<TestResult>> topicChoice(Map<String, ArrayList<TestResult>> results, String topic) {
        Map<String, ArrayList<TestResult>> multiMap = new HashMap<String, ArrayList<TestResult>>();
        multiMap.put(topic, results.get(topic));
        return multiMap;
    }


    public void sortTestResults(Map<String, ArrayList<TestResult>> results, String sortType) {
        switch (sortType) {
            case "name":
                results.forEach((s, testResults) -> testResults.sort(Comparator.comparing(n -> n.getTestModule().getName())));
                //results.sort(Comparator.comparing(n -> n.getTestModule().getName()));
                break;
            case "topic":
                results.forEach((s, testResults) -> testResults.sort(Comparator.comparing(n -> n.getTestModule().getTopic())));
                //    results.sort(Comparator.comparing(n -> n.getTestModule().getTopic()));
                break;
            case "questions":
                results.forEach((s, testResults) -> testResults.sort(Comparator.comparing(n -> n.getTestModule().getQuestions().size())));
                // results.sort(Comparator.comparing(n -> n.getTestModule().getQuestions().size()));
                break;
            case "hard":
                results.forEach((s, testResults) -> testResults.sort(Comparator.comparing(n -> n.getTestModule().getHardLevel())));
                //   results.sort(Comparator.comparing(n -> n.getTestModule().getHardLevel()));
                break;
            default:
                throw new IllegalArgumentException();
        }
    }

    public int getAverageScore(List<TestResult> results) {
        if (results.size() > 0) {
            OptionalDouble poinstSum = results.stream().filter(testResult ->
                    testResult.getIsDone()).mapToInt(TestResult::getPoints).average();
            return (int) poinstSum.getAsDouble();
        } else return 0;
    }
}
