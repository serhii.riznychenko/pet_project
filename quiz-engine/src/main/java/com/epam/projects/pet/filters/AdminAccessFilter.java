package com.epam.projects.pet.filters;

import com.epam.projects.pet.models.User;
import org.apache.commons.lang.StringUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Filter that processes incoming administrator requests
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.1
 */
@WebFilter(
        urlPatterns = "/admin/*",
        filterName = "AdminFilter",
        description = "Filter all admin URLs"
)
public class AdminAccessFilter implements Filter {

    private List<String> adminAccessPath;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        adminAccessPath = Arrays.asList("admin", "tests", "questions", "answers", "update", "create", "delete", "results");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {

        String uri = ((HttpServletRequest) request).getRequestURI();
        String pathWithParameters = StringUtils.substringAfterLast(uri, "/");
        String path = StringUtils.substringBefore(pathWithParameters, "?");

        if (!adminAccessPath.contains(path)) {
            filterChain.doFilter(request, response);
            return;
        }

        HttpSession session = ((HttpServletRequest) request).getSession();
        User user = (User) session.getAttribute("loggedUser");

        if (user != null && user.getRole().equals(User.Role.ADMIN)) {
            filterChain.doFilter(request, response);
            return;
        }
        ((HttpServletResponse) response).sendRedirect("/quiz-engine/login");
    }

    @Override
    public void destroy() {
        adminAccessPath = null;
    }
}
