package com.epam.projects.pet.controllers.admin;

import com.epam.projects.pet.models.TestResult;
import com.epam.projects.pet.services.TestModuleService;
import com.epam.projects.pet.services.TestResultService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * TestResult view and delete servlet
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.0
 */
@WebServlet(
        name = "TestResultList",
        urlPatterns = "/admin/results"
)
public class TestResultList extends HttpServlet {
    private TestResultService testResultService;
    private TestModuleService testModuleService;

    @Override
    public void init() throws ServletException {
        testResultService = new TestResultService();
        testModuleService = new TestModuleService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            List<TestResult> allResults = testResultService.getAllTestResults();
            for (TestResult result: allResults) {
                if (result.getTestID() > 0) {
                    result.setTestModule(testModuleService.getTest(result.getTestID()));
                }
            }
            request.setAttribute("resultList", allResults);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/admin/testsResults.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int resultID = Integer.parseInt(request.getParameter("resultID"));
        try {
            testResultService.deleteTestResult(resultID);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            response.sendRedirect("/quiz-engine/admin/results");
        }
    }
}
