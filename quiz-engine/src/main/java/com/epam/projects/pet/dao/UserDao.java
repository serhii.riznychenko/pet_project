package com.epam.projects.pet.dao;

import com.epam.projects.pet.models.User;

import java.io.CharArrayReader;
import java.io.Reader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserDao extends AbstractDao<User, Integer> {

    public UserDao() {
    }

    @Override
    public ArrayList<User> getAll() throws SQLException {
        ArrayList<User> users = new ArrayList<>();
        String SELECT_ALL_USERS = "select * from users";
        try (PreparedStatement ps = getPrepareStatement(SELECT_ALL_USERS)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setUserID(rs.getInt(1));
                user.setLogin(rs.getString(2));
                user.setEmail(rs.getString(3));
                user.setRealName(rs.getString(5));
                user.setIsBanned(rs.getBoolean(6));
                user.setAge(rs.getInt(7));
                user.setRole(User.Role.valueOf(rs.getString(8)));
                users.add(user);
            }
        }
        return users;
    }

    @Override
    public User update(User entity) throws SQLException {
        String UPDATE_USER = "update users set login = ?, email = ?, password = ?, real_name = ?, age = ?, role = ? where userID = ?";
        try (PreparedStatement ps = getPrepareStatement(UPDATE_USER)) {
            ps.setString(1, entity.getLogin());
            ps.setString(2, entity.getEmail());
            Reader reader = new CharArrayReader(entity.getPassword());
            ps.setCharacterStream(3, reader);
            ps.setString(4, entity.getRealName());
            ps.setInt(5, entity.getAge());
            ps.setString(6, entity.getRole().name());
            ps.setInt(7, entity.getUserID());
            ps.executeUpdate();
        }
        return entity;
    }

    @Override
    public User getEntityById(Integer id) throws SQLException {
        String SELECT_USER = "select * from users where userID = ?";
        User user = new User();
        try (PreparedStatement ps = getPrepareStatement(SELECT_USER)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user.setUserID(rs.getInt(1));
                user.setLogin(rs.getString(2));
                user.setEmail(rs.getString(3));
                user.setPassword(rs.getString(4).toCharArray());
                user.setRealName(rs.getString(5));
                user.setIsBanned(rs.getBoolean(6));
                user.setAge(rs.getInt(7));
                user.setRole(User.Role.valueOf(rs.getString(8)));
            }
        }
        return user;
    }

    @Override
    public boolean delete(Integer id) throws SQLException {
        String DELETE_USER = "delete from users where userID = ?";
        try (PreparedStatement ps = getPrepareStatement(DELETE_USER)) {
            ps.setInt(1, id);
            ps.executeUpdate();
        }
        return true;
    }

    @Override
    public boolean create(User entity) throws SQLException {
        String CREATE_USER = "insert into users (login, email, password, real_name, age, role) values (?, ?, ?, ?, ?, ?)";
        try (PreparedStatement ps = getPrepareStatement(CREATE_USER)) {
            ps.setString(1, entity.getLogin());
            ps.setString(2, entity.getEmail());
            Reader reader = new CharArrayReader(entity.getPassword());
            ps.setCharacterStream(3, reader);
            ps.setString(4, entity.getRealName());
            ps.setInt(5, entity.getAge());
            ps.setString(6, entity.getRole().name());
            ps.executeUpdate();
        }
        return true;
    }

    public User getEntityByLogin(String login) throws SQLException {
        String SELECT_USER_BY_LOGIN = "select * from users where login = ?";
        User user = new User();
        try (PreparedStatement ps = getPrepareStatement(SELECT_USER_BY_LOGIN)) {
            ps.setString(1, login);
            ResultSet rs = ps.executeQuery();
            rs.next();
            user.setUserID(rs.getInt(1));
            user.setLogin(rs.getString(2));
            user.setEmail(rs.getString(3));
            user.setPassword(rs.getString(4).toCharArray());
            user.setRealName(rs.getString(5));
            user.setIsBanned(rs.getBoolean(6));
            user.setAge(rs.getInt(7));
            user.setRole(User.Role.valueOf(rs.getString(8)));
        }
        return user;
    }

    public boolean banUserOp(int id, boolean ban) throws SQLException {
        String BAN_USER = "update users set is_banned = ? where userID = ?";
        try (PreparedStatement ps = getPrepareStatement(BAN_USER)) {
            ps.setBoolean(1, ban);
            ps.setInt(2, id);
            ps.executeUpdate();
        }
        return true;
    }

    public char[] getPassword(int id) throws SQLException {
        String SELECT_PASSWORD_BY_ID = "select password from users where userID = ?";
        char[] password;
        try (PreparedStatement ps = getPrepareStatement(SELECT_PASSWORD_BY_ID)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            rs.next();
            password = rs.getString(1).toCharArray();
        }
        return password;
    }

    public boolean updatePassword(int id, char[] pass) throws SQLException {
        String UPDATE_USER_PASSWORD = "update users set password = ? where userID = ?";
        try (PreparedStatement ps = getPrepareStatement(UPDATE_USER_PASSWORD)) {
            ps.setCharacterStream(1, new CharArrayReader(pass));
            ps.setInt(2, id);
            ps.executeUpdate();
        }
        return true;
    }

/*    public boolean updateRole(int id, String role) throws SQLException {
        String UPDATE_USER_PASSWORD = "update users set role = ? where userID = ?";
        try (PreparedStatement ps = getPrepareStatement(UPDATE_USER_PASSWORD)) {
            ps.setString(1, role);
            ps.setInt(2, id);
            ps.executeUpdate();
        }
        return true;
    }*/
}
