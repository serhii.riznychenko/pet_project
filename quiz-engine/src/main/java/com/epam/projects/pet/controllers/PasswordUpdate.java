package com.epam.projects.pet.controllers;

import com.epam.projects.pet.models.User;
import com.epam.projects.pet.services.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(
        name = "PasswordUpdate",
        urlPatterns = "/account/password/update"
)
public class PasswordUpdate extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(ProfileDelete.class);
    AccountService accountService = new AccountService();


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/passwordUpdate.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String lastPass = request.getParameter("oldPass");
        String newPass = request.getParameter("newPass");
        User user = (User) request.getSession().getAttribute("loggedUser");

        if (lastPass != null && newPass != null) {
            try {
                accountService.updateUserPassword(lastPass, newPass, user);
            } catch (SQLException e) {
                LOG.error(e.getMessage());
                request.setAttribute("error", "database_error");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/passwordUpdate.jsp");
                dispatcher.forward(request, response);
                return;

            } catch (IllegalArgumentException e) {
                LOG.warn(e.getMessage());
                request.setAttribute("error", "old_password_is_incorrect");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/passwordUpdate.jsp");
                dispatcher.forward(request, response);
                return;
            }
        }
        response.sendRedirect("/quiz-engine/profile");
    }
}
