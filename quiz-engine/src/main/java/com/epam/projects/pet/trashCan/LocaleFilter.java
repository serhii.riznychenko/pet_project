package com.epam.projects.pet.trashCan;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;

public class LocaleFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest)request).getSession();
        Locale locale = (Locale) session.getAttribute("LOCALE");

        if (locale != null) {
            filterChain.doFilter(request, response);
            return;
        }
        if (request.getLocale() == null) {
            session.setAttribute("LOCALE", new Locale("en", "US"));
        } else {
            session.setAttribute("LOCALE", request.getLocale());
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
