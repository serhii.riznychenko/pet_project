package com.epam.projects.pet.services;

import com.epam.projects.pet.dao.AnswerDao;
import com.epam.projects.pet.dao.QuestionDao;
import com.epam.projects.pet.models.Answer;
import com.epam.projects.pet.models.Question;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to operate with test questions
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.1
 */
public class QuestionService {

    public Question createQuestion(String question, int testID) throws SQLException {
        QuestionDao questionDao = new QuestionDao();
        try {
            Question newQuestion = new Question();
            newQuestion.setQuestion(question);
            newQuestion.setTestID(testID);
            questionDao.create(newQuestion);
            return newQuestion;
        } finally {
            questionDao.returnConnection();
        }
    }

    public Question getQuestion(int questionID) throws SQLException {
        QuestionDao questionDao = new QuestionDao();
        try {
            return questionDao.getEntityById(questionID);
        } finally {
            questionDao.returnConnection();
        }
    }

    public Question updateQuestion(int questionID, String question, int testID) throws SQLException {
        QuestionDao questionDao = new QuestionDao();
        try {
            Question newQuestion = new Question();
            newQuestion.setQuestionID(questionID);
            newQuestion.setQuestion(question);
            newQuestion.setTestID(testID);
            questionDao.update(newQuestion);
            return newQuestion;
        } finally {
            questionDao.returnConnection();
        }
    }

    public void deleteQuestion(int questionID) throws SQLException {
        QuestionDao questionDao = new QuestionDao();
        AnswerDao answerDao = new AnswerDao();
        AnswerService answerService = new AnswerService();
        try {
            List<Answer> answers = answerService.getAnswersByQuestionID(questionID);
            for (Answer answer : answers) {
                answerDao.delete(answer.getAnswerID());
            }
            questionDao.delete(questionID);
        } finally {
            answerDao.returnConnection();
            questionDao.returnConnection();
        }
    }

    /**
     * Loads all questions by the value of the testID, then loads each answer in question by the value of questionID
     *
     * @param testID Id number of the test
     * @return List of questions
     * @throws SQLException
     */
    public ArrayList<Question> loadListOfAnswersIntoListOfQuestions(int testID) throws SQLException {
        QuestionDao questionDao = new QuestionDao();
        AnswerDao answerDao = new AnswerDao();
        try {
            ArrayList<Question> questions = questionDao.getQuestionsByTestId(testID);
            for (Question question : questions) {
                if (answerDao.getAnswersByQuestionId(question.getQuestionID()) != null) {
                    question.setAnswers(answerDao.getAnswersByQuestionId(question.getQuestionID()));
                }
            }
            return questions;
        } finally {
            questionDao.returnConnection();
            answerDao.returnConnection();
        }
    }
}
