package com.epam.projects.pet.controllers;

import com.epam.projects.pet.models.User;
import com.epam.projects.pet.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(
        name = "ProfileDelete",
        urlPatterns = "/profile/delete"
)
public class ProfileDelete extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(ProfileDelete.class);
    UserService userService = new UserService();


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/profileDelete.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = (User) request.getSession().getAttribute("loggedUser");
        try {
            if (userService.deleteUser(user)) {
                request.getSession().setAttribute("loggedUser", null);
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        response.sendRedirect("/quiz-engine/login");
    }
}
