package com.epam.projects.pet.controllers.admin;

import com.epam.projects.pet.models.Answer;
import com.epam.projects.pet.services.AnswerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Answer update servlet
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.0
 */
@WebServlet(
        name = "AnswerUpdate",
        urlPatterns = "/admin/tests/questions/answers/update"
)
public class AnswerUpdate extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(AnswerUpdate.class);
    private AnswerService answerService;

    @Override
    public void init() throws ServletException {
        answerService = new AnswerService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int answerID = Integer.parseInt(request.getParameter("answerID"));

        try {
            Answer answer = answerService.getAnswer(answerID);
            request.setAttribute("answer", answer);

        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/admin/answerUpdate.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int answerID = Integer.parseInt(request.getParameter("answerID"));
        int testID = Integer.parseInt(request.getParameter("testID"));
        String answer = request.getParameter("answer");
        boolean isCorrect = Boolean.parseBoolean(request.getParameter("isCorrect"));
        int questionID = Integer.parseInt(request.getParameter("questionID"));

        try {
            answerService.updateAnswer(answerID, answer, isCorrect, questionID);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        } finally {
            response.sendRedirect("/quiz-engine/admin/tests/questions/update?testID=" + testID + "&questionID=" + questionID);
        }
    }
}
