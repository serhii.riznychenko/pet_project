package com.epam.projects.pet.filters;

import com.epam.projects.pet.models.User;
import org.apache.commons.lang.StringUtils;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


/**
 * Filter that processes all incoming requests,
 * if user is not authorized, redirects him to login page
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.1
 */
@WebFilter(
        urlPatterns = "/*",
        filterName = "AuthenticationFilter"
)
public class AuthenticationFilter implements Filter {

    private List<String> accessPath;
    private List<String> blockedPath;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        accessPath = Arrays.asList("home", "tests", "testing", "sorting", "topic", "profile", "update", "delete");
        blockedPath = Arrays.asList("login", "registration", "");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {

        String uri = ((HttpServletRequest) request).getRequestURI();

        String pathWithParameters = StringUtils.substringAfterLast(uri, "/");
        String path = StringUtils.substringBefore(pathWithParameters, "?");

        HttpSession session = ((HttpServletRequest) request).getSession();
        User user = (User) session.getAttribute("loggedUser");

        if (blockedPath.contains(path) && user != null) {
            ((HttpServletResponse) response).sendRedirect("/quiz-engine/home");
            return;
        }

        if (!accessPath.contains(path)) {
            filterChain.doFilter(request, response);
            return;
        }

        if (user != null) {
            if (user.getRole().equals(User.Role.ADMIN)) {
                if (uri.matches("/quiz-engine/home")) {
                    ((HttpServletResponse) response).sendRedirect("/quiz-engine/admin");
                    return;
                }
            }
            filterChain.doFilter(request, response);
            return;
        }
        ((HttpServletResponse) response).sendRedirect("/quiz-engine/login");
    }

    @Override
    public void destroy() {
        accessPath = null;
        blockedPath = null;
    }
}
