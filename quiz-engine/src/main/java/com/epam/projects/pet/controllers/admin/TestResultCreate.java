package com.epam.projects.pet.controllers.admin;

import com.epam.projects.pet.services.TestResultService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * TestResult create servlet
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.0
 */
@WebServlet(
        name = "TestResultCreate",
        urlPatterns = "/admin/results/create"
)
public class TestResultCreate extends HttpServlet {
    private TestResultService testResultService;

    @Override
    public void init() throws ServletException {
        testResultService = new TestResultService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/admin/testResultCreate.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int userID = Integer.parseInt(request.getParameter("userID"));
        int testID = Integer.parseInt(request.getParameter("testID"));
        try {
            testResultService.createTestResult(userID, testID);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            response.sendRedirect("/quiz-engine/admin/results");
        }

    }
}
