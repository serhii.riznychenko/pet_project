package com.epam.projects.pet.controllers.admin;

import com.epam.projects.pet.services.QuestionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Question create servlet
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.0
 */
@WebServlet(
        name = "QuestionCreate",
        urlPatterns = "/admin/tests/questions/create"
)
public class QuestionCreate extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(QuestionCreate.class);
    private QuestionService questionService;

    @Override
    public void init() throws ServletException {
        questionService = new QuestionService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/admin/questionCreate.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String question = request.getParameter("question");
        int testID = Integer.parseInt(request.getParameter("testID"));

        try {
            questionService.createQuestion(question, testID);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
            //TODO redirect to 404
        } finally {
            response.sendRedirect("/quiz-engine/admin/tests/update?testID=" + testID);
        }
    }
}

