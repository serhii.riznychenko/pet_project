package com.epam.projects.pet.tags;

import com.epam.projects.pet.utils.UTF8Control;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import java.io.*;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * A custom tag for localizing text on jsp pages
 */
public class LocalizationTag extends TagSupport {
    private UTF8Control utf8Control = new UTF8Control();
    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int doStartTag() throws JspException {
        HttpSession session = pageContext.getSession();

        if (session.getAttribute("LOCALE") == null) {
            if (pageContext.getRequest().getLocale() == null) {
                session.setAttribute("LOCALE", new Locale("en", "US"));
            } else {
                session.setAttribute("LOCALE", pageContext.getRequest().getLocale());
            }
        }

        Locale locale = (Locale) session.getAttribute("LOCALE");

        if (message != null && !message.isEmpty()) {
            ResourceBundle messages = ResourceBundle.getBundle("i18n.messages", locale, utf8Control);
            String locMessage = messages.getString(message);
            try {
                pageContext.getOut().print(locMessage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return SKIP_BODY;
    }
}
