package com.epam.projects.pet.controllers.admin;

import com.epam.projects.pet.models.TestModule;
import com.epam.projects.pet.services.TestModuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Servlet for showing and deleting tests
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.1
 */
@WebServlet(
        name = "TestList",
        urlPatterns = "/admin/tests"
)
public class TestList extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(TestList.class);
    private TestModuleService testModuleService;

    @Override
    public void init() throws ServletException {
        testModuleService = new TestModuleService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<TestModule> testList = testModuleService.getAllTests();
            testModuleService.loadSetOfQuestionsToTestModule(testList);
            request.setAttribute("tests", testList);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
            response.sendRedirect("/error-pages/databaseError.jsp");
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/admin/testList.jsp");
        dispatcher.forward(request, response);
    }

    /**
     * Deletes test
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int testID = Integer.parseInt(request.getParameter("testID"));
        try {
            testModuleService.deleteTest(testID);
        } catch (SQLException e) {
            e.printStackTrace();
            response.sendRedirect("/error-pages/nothingFound.jsp");
        }
        doGet(request, response);
    }
}
