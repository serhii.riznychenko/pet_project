package com.epam.projects.pet.trashCan;

import com.epam.projects.pet.services.TestResultService;
import com.epam.projects.pet.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;



public class TestingAccessFilter implements Filter {

    static final Logger LOG = LoggerFactory.getLogger(TestingAccessFilter.class);
    private UserService userService;
    private TestResultService testResultService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        userService = new UserService();
        testResultService = new TestResultService();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {

        Integer testResultID = Integer.parseInt(request.getParameter("testResultID"));
        HttpSession session = ((HttpServletRequest) request).getSession();


    }

    @Override
    public void destroy() {

    }
}
