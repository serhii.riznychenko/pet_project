package com.epam.projects.pet.services;

import com.epam.projects.pet.dao.UserDao;
import com.epam.projects.pet.models.User;
import com.epam.projects.pet.utils.Validator;
import com.epam.projects.pet.utils.Password;

import java.sql.SQLException;

/**
 * Class to operate with user accounts
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.1
 */
public class AccountService {

    /**
     * First checks the user's data, if all the data is valid, creates a new user
     *
     * @param login    User login
     * @param email    User email
     * @param username User name
     * @param password User password
     * @param age      User age
     * @return Created user
     * @throws SQLException             User with same login exists
     * @throws IllegalArgumentException Incorrect input data
     * @author Serhii Riznychenko <magnesijium@gmail.com>
     * @version 1.2
     */
    public User createUserAccount(String login, String email, String username, String password, String age, User.Role role)
            throws SQLException {
        UserDao userDao = new UserDao();
        User user = new User();
        user.setLogin(login);
        user.setEmail(email);
        user.setRealName(username);
        user.setAge(Integer.parseInt(age));
        String hash = Password.hashPassword(password);
        user.setPassword(hash.toCharArray());
        user.setRole(role);
        try {
            userDao.create(user);
            return user;
        } finally {
            userDao.returnConnection();
        }
    }

    /**
     * Checks the user's login and pass valid, get user by login and check it's password by hash, if they are same, returns user
     *
     * @param login Logged user login
     * @param pass  Logged user password
     * @return User, found in the database
     * @throws IllegalAccessException   Incorrect password
     * @throws IllegalArgumentException Incorrect input data
     * @throws SQLException             Found no user by login
     */
    public User getUserAccount(String login, String pass) throws IllegalAccessException, SQLException {

        UserDao userDao = new UserDao();
        try {
            User user = userDao.getEntityByLogin(login);

            if (Password.checkPassword(pass, String.valueOf(user.getPassword()))) {
                return user;
            } else throw new IllegalAccessException("Incorrect password");
        } finally {
            userDao.returnConnection();
        }
    }

    public void updateUserPassword(String oldPass,String newPass, User user) throws IllegalArgumentException, SQLException {
        UserDao userDao = new UserDao();
        try {
            if (Password.checkPassword(oldPass, String.valueOf(user.getPassword()))) {
               user.setPassword(Password.hashPassword(newPass).toCharArray());
               userDao.update(user);
            } else throw new IllegalArgumentException("Incorrect password");
        } finally {
            userDao.returnConnection();
        }
    }

    public boolean checkUserBanStatus(User user) {
        return user.getIsBanned();
    }
}
