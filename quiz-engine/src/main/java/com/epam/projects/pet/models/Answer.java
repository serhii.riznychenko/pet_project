package com.epam.projects.pet.models;

import java.io.Serializable;

public class Answer implements Serializable {
    private static final long serialVersionUID = -7462894482697640005L;

    private int answerID;
    private String answer;
    private boolean isCorrect;
    private int questionID;

    public Answer() {}

    public int getAnswerID() {
        return answerID;
    }

    public void setAnswerID(int answerID) {
        this.answerID = answerID;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    @Override
    public int hashCode() {
        int result = answerID;
        result = 42 * result + (answer != null ? answer.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Answer {" +
                "answer id='" + answerID + '\'' +
                ", answer='" + answer + '\'' +
                ", isCorrect='" + isCorrect + '\'' +
                ", question id='" + questionID + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Answer answerM = (Answer) o;
        if (answer != null ? !answer.equals(answerM.answer) : answerM.answer != null) return false;
        if (!isCorrect == answerM.isCorrect) return false;
        return questionID >= 0 ? questionID == (answerM.questionID) : false;
    }
}
