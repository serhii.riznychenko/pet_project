package com.epam.projects.pet.models;

import java.io.Serializable;
import java.util.ArrayList;

public class TestModule implements Serializable {
    private static final long serialVersionUID = -5101136013037562450L;

    private int testID;
    private String name;
    private int hardLevel;
    private String topic;
    private int timeToSolve;
    private ArrayList<Question> questions;

    public TestModule() {}

    public int getTestID() {
        return testID;
    }

    public void setTestID(int testID) {
        this.testID = testID;
    }

    public int getHardLevel() {
        return hardLevel;
    }

    public void setHardLevel(int hardLevel) {
        this.hardLevel = hardLevel;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public int getTimeToSolve() {
        return timeToSolve;
    }

    public void setTimeToSolve(int timeToSolve) {
        this.timeToSolve = timeToSolve;
    }


    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int result = testID;
        result = 42 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Test module {" +
                "test id='" + testID + '\'' +
                ", test name='" + name + '\'' +
                ", hard level='" + hardLevel + '\'' +
                ", topic='" + topic + '\'' +
                ", time to solve='" + timeToSolve + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestModule testModule = (TestModule) o;
        if (testModule != null ? !name.equals(testModule.name) : testModule.name != null) return false;
        if (testModule != null ? !name.equals(testModule.topic) : testModule.topic != null) return false;
        if (hardLevel != 0 ? hardLevel != testModule.hardLevel : testModule.hardLevel != 0) return false;
        return timeToSolve >= 0 ? timeToSolve == (testModule.timeToSolve) : false;
    }
}
