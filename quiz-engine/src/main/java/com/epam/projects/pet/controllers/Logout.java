package com.epam.projects.pet.controllers;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * * Servlet, which deals with users logout
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.2
 */
@WebServlet(
        name = "Logout",
        urlPatterns = "/logout"
)
public class Logout extends HttpServlet {

    /**
     * Deletes a user session and redirects to login page
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getSession().invalidate();
        response.sendRedirect("/quiz-engine/login");
    }
}