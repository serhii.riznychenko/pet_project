package com.epam.projects.pet.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Question implements Serializable {
    private static final long serialVersionUID = 8263620259119540272L;

    private int questionID;
    private String question;
    private int testID;
    private ArrayList<Answer> answers;

    public Question() {}

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getTestID() {
        return testID;
    }

    public void setTestID(int testID) {
        this.testID = testID;
    }

    public ArrayList<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<Answer> answers) {
        this.answers = answers;
    }

    @Override
    public int hashCode() {
        int result = questionID;
        result = 42 * result + (question != null ? question.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Question {" +
                "question id='" + questionID + '\'' +
                ", question='" + question + '\'' +
                ", test ID='" + testID + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question questionM = (Question) o;
        if (question != null ? !question.equals(questionM.question) : questionM.question != null) return false;
        return testID >= 0 ? testID == (questionM.testID) : false;
    }
}
