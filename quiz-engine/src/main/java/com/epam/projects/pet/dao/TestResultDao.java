package com.epam.projects.pet.dao;

import com.epam.projects.pet.models.TestResult;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TestResultDao extends AbstractDao<TestResult, Integer> {

    public TestResultDao() {
    }

    @Override
    public List<TestResult> getAll() throws SQLException {
        ArrayList<TestResult> results = new ArrayList<>();
        String SELECT_ALL_RESULTS = "select * from results";
        PreparedStatement ps = getPrepareStatement(SELECT_ALL_RESULTS);
        try {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TestResult result = new TestResult();
                result.setResultID(rs.getInt(1));
                result.setUserID(rs.getInt(2));
                result.setTestID(rs.getInt(3));
                result.setIsDone(rs.getBoolean(4));
                result.setPoints(rs.getInt(5));
                results.add(result);
            }
        } finally {
            closePrepareStatement(ps);
        }
        return results;
    }

    @Override
    public TestResult update(TestResult entity) throws SQLException {
        String UPDATE_RESULT = "update results set userID = ?, testID = ?, is_done = ?, points = ? where resultID = ?";
        PreparedStatement ps = getPrepareStatement(UPDATE_RESULT);
        try {
            ps.setInt(1, entity.getUserID());
            ps.setInt(2, entity.getTestID());
            ps.setBoolean(3, entity.getIsDone());
            ps.setInt(4, entity.getPoints());
            ps.setInt(5, entity.getResultID());
            ps.executeUpdate();
        } finally {
            closePrepareStatement(ps);
        }
        return entity;
    }

    @Override
    public TestResult getEntityById(Integer id) throws SQLException {
        String SELECT_RESULT = "select * from results where resultID = ?";
        PreparedStatement ps = getPrepareStatement(SELECT_RESULT);
        TestResult result = new TestResult();
        try {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                result.setResultID(id);
                result.setUserID(rs.getInt(2));
                result.setTestID(rs.getInt(3));
                result.setIsDone(rs.getBoolean(4));
                result.setPoints(rs.getInt(5));
            }
        } finally {
            closePrepareStatement(ps);
        }
        return result;
    }

    @Override
    public boolean delete(Integer id) throws SQLException {
        String DELETE_RESULT = "delete from results where resultID = ?";
        PreparedStatement ps = getPrepareStatement(DELETE_RESULT);
        try {
            ps.setInt(1, id);
            ps.executeUpdate();
        } finally {
            closePrepareStatement(ps);
        }
        return true;
    }

    @Override
    public boolean create(TestResult entity) throws SQLException {
        String CREATE_RESULT = "insert into results (userID, testID, is_done, points) values (?, ?, ?, ?)";
        PreparedStatement ps = getPrepareStatement(CREATE_RESULT);
        try {
            ps.setInt(1, entity.getUserID());
            ps.setInt(2, entity.getTestID());
            ps.setBoolean(3, entity.getIsDone());
            ps.setInt(4, entity.getPoints());
            ps.executeUpdate();
        } finally {
            closePrepareStatement(ps);
        }
        return true;
    }

    public ArrayList<TestResult> getResultsByUserId(Integer userId) throws SQLException {
        String SELECT_RESULTS_BY_USERID = "select * from results where userID = ?";
        PreparedStatement ps = getPrepareStatement(SELECT_RESULTS_BY_USERID);
        ArrayList<TestResult> results = new ArrayList<>();
        try {
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                TestResult result = new TestResult();
                result.setResultID(rs.getInt(1));
                result.setUserID(userId);
                result.setTestID(rs.getInt(3));
                result.setIsDone(rs.getBoolean(4));
                result.setPoints(rs.getInt(5));
                results.add(result);
            }
        } finally {
            closePrepareStatement(ps);
        }
        return results;
    }

/*    public TestResult getEntityByUserAndTestId(Integer userID, Integer testID) throws SQLException {
        String SELECT_RESULT_BY_USETID_TESTID = "select * from results where where userID = ? AND testID = ?";
        PreparedStatement ps = getPrepareStatement(SELECT_RESULT_BY_USETID_TESTID);
        TestResult result = new TestResult();
        try {
            ps.setInt(1, userID);
            ps.setInt(1, testID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                result.setUserID(rs.getInt(2));
                result.setTestID(rs.getInt(3));
                result.setIsDone(rs.getBoolean(4));
                result.setPoints(rs.getInt(5));
            }
        } finally {
            closePrepareStatement(ps);
        }
        return result;
    }*/
}
