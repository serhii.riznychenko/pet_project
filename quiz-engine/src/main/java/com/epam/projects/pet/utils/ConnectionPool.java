package com.epam.projects.pet.utils;

import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Connection pool is a thread-safe singleton using the BonesCP library as a pool of connections
 *
 * @param propertyPath Path to database property file
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.0
 */
public class ConnectionPool {

    private static Logger LOG = LoggerFactory.getLogger(ConnectionPool.class);
    private static volatile ConnectionPool instance;
    private BoneCP connectionPool;
    private static volatile String propertyPath = "liquibase/liquibase.properties";

    public static void setPropertyPath(String newPropertyPath) {
        propertyPath = newPropertyPath;
    }

    private BoneCPConfig getConfig() throws IOException, ClassNotFoundException {
        Properties props = new Properties();
        try (InputStream in = ConnectionPool.class.getClassLoader().getResourceAsStream(propertyPath)) {
            props.load(in);
        }

        BoneCPConfig config = new BoneCPConfig();
        String drivers = props.getProperty("driver");
        Class.forName(drivers);
        config.setJdbcUrl(props.getProperty("url"));
        config.setUsername(props.getProperty("username"));
        config.setPassword(props.getProperty("password"));
        config.setMinConnectionsPerPartition(1);
        config.setMaxConnectionsPerPartition(20);
        config.setPartitionCount(10);
        return config;
    }

    private ConnectionPool() throws SQLException {
        try {
            connectionPool = new BoneCP(getConfig());
        } catch (IOException | ClassNotFoundException e) {
            LOG.error(e.getMessage());
        }
    }

    public static ConnectionPool getInstance() {
        ConnectionPool localInstance = instance;
        if (localInstance == null) {
            synchronized (ConnectionPool.class) {
                localInstance = instance;
                if (localInstance == null) {
                    try {
                        instance = localInstance = new ConnectionPool();
                    } catch (SQLException e) {
                        LOG.error(e.getMessage());
                    }
                }
            }
        }
        return localInstance;
    }

    public Connection getConnection() {
        try {
            return connectionPool.getConnection();
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
        throw new RuntimeException("Can not create connection");
    }

    public void shutdown() {
        connectionPool.shutdown();
    }
}