package com.epam.projects.pet.controllers.admin;

import com.epam.projects.pet.services.TestModuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Creates test module
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.1
 */
@WebServlet(
        name = "TestModuleCreate",
        urlPatterns = "/admin/tests/create"
)
public class TestModuleCreate extends HttpServlet {
    private static final Logger LOG = LoggerFactory.getLogger(TestModuleCreate.class);
    private TestModuleService testModuleService;

    @Override
    public void init() throws ServletException {
        testModuleService = new TestModuleService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/admin/testModuleCreate.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String testName = request.getParameter("name");
        int testHardLevel = Integer.parseInt(request.getParameter("hardLevel"));
        String testTopic = request.getParameter("topic");
        int timeToSolveTest = Integer.parseInt(request.getParameter("timeToSolve"));

        try {
            testModuleService.createTest(testName, testHardLevel, testTopic, timeToSolveTest);
        } catch (SQLException e) {
            LOG.error(e.getMessage());
            response.sendRedirect("/error-pages/databaseError.jsp");
        }
        response.sendRedirect("/quiz-engine/admin/tests");
    }
}
