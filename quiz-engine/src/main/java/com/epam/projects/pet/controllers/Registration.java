package com.epam.projects.pet.controllers;

import com.epam.projects.pet.models.User;
import com.epam.projects.pet.services.AccountService;
import com.epam.projects.pet.utils.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Servlet, which deals with users registration
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.2
 */
@WebServlet(
        name = "Registration",
        urlPatterns = "/registration"
)
public class Registration extends HttpServlet {

    static final Logger LOG = LoggerFactory.getLogger(Registration.class);
    private AccountService accountService;
    private Validator validator;

    @Override
    public void init() throws ServletException {
        accountService = new AccountService();
        validator = new Validator();
    }

    /**
     * Redirects the user to the registration page
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/registration.jsp");
        dispatcher.forward(request, response);
    }

    /**
     * Checks the entered data and creates a new user,
     * sets max interact session time for 24 hours
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession();
        try {
            String login = request.getParameter("login");
            String email = request.getParameter("email");
            String userName = request.getParameter("userName");
            String password = request.getParameter("password");
            String age = request.getParameter("age");

            if (!validator.isLoginValid(login)
                    || !validator.isEmailValid(email)
                    || !validator.isNameValid(userName)
                    || !validator.isPasswordValid(password)
                    || !validator.isAgeValid(age)) {
                LOG.warn("Invalid registration data");
                request.setAttribute("error", "invalid_data");
                doGet(request, response);
                return;
            }

            User user = accountService.createUserAccount(login, email, userName, password, age, User.Role.USER);

            session.setAttribute("loggedUser", user);
            session.setMaxInactiveInterval(24 * 60 * 60);
            response.sendRedirect("home");

        } catch (SQLException e) {
            LOG.info(e.getMessage());
            LOG.warn(e.getMessage());
            request.setAttribute("error", "user_already_exists");
            doGet(request, response);
            return;
        }
    }
}
