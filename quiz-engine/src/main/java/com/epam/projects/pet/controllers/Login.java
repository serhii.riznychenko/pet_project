package com.epam.projects.pet.controllers;

import com.epam.projects.pet.models.User;
import com.epam.projects.pet.services.AccountService;
import com.epam.projects.pet.services.UserService;
import com.epam.projects.pet.utils.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Servlet, which deals with users login
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.2
 */
@WebServlet(
        name = "LoginServlet",
        urlPatterns = "/login"
)
public class Login extends HttpServlet {

    private static final Logger LOG = LoggerFactory.getLogger(Login.class);
    private UserService userService;
    private AccountService accountService;
    private Validator validator;

    @Override
    public void init() throws ServletException {
        userService = new UserService();
        accountService = new AccountService();
        validator = new Validator();
    }

    /**
     * Redirects the user to the login page
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
        dispatcher.forward(request, response);
        return;
    }

    /**
     * Checks the entered user data and creates a user session,
     * sets max interact session time for 24 hours
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        HttpSession session = request.getSession();

        String login = request.getParameter("login");
        String pass = request.getParameter("password");

        if (!validator.isLoginValid(login)
                || !validator.isPasswordValid(pass)) {
            LOG.warn("Invalid characters");
            request.setAttribute("error", "incorrect_login_data");
            doGet(request, response);
            return;
        }
        try {
            User user = accountService.getUserAccount(request.getParameter("login"),
                    request.getParameter("password"));

            if (accountService.checkUserBanStatus(user)) {
                request.setAttribute("error", "user_is_banned");
                doGet(request, response);
                return;
            }
            session.setAttribute("loggedUser", user);
            session.setMaxInactiveInterval(24 * 60 * 60);
            response.sendRedirect("home");

        } catch (IllegalArgumentException| IllegalAccessException e) {
            request.setAttribute("error", "incorrect_login_data");
            doGet(request, response);

        } catch (SQLException sqlex) {
            LOG.warn(sqlex.getMessage());
            request.setAttribute("error", "found_no_user_by_login");
            doGet(request, response);
        }
    }
}
