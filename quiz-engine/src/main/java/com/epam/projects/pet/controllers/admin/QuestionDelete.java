package com.epam.projects.pet.controllers.admin;

import com.epam.projects.pet.services.QuestionService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Question delete servlet
 *
 * @author Serhii Riznychenko <magnesijium@gmail.com>
 * @version 1.0
 */
@WebServlet(
        name = "QuestionDelete",
        urlPatterns = "/admin/tests/questions/delete"
)
public class QuestionDelete extends HttpServlet {
    private QuestionService questionService;

    @Override
    public void init() throws ServletException {
        questionService = new QuestionService();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int questionID = Integer.parseInt(request.getParameter("questionID"));
        int testID = Integer.parseInt(request.getParameter("testID"));

        try {
            questionService.deleteQuestion(questionID);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        response.sendRedirect("/quiz-engine/admin/tests/update?testID=" + testID);
    }
}
