package com.epam.projects.pet.dao;

import com.epam.projects.pet.models.Question;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class QuestionDao extends AbstractDao<Question, Integer> {

    public QuestionDao() {
    }

    @Override
    public List<Question> getAll() throws SQLException {
        ArrayList<Question> questions = new ArrayList<>();
        String SELECT_ALL_QUESTIONS = "select * from questions";
        PreparedStatement ps = getPrepareStatement(SELECT_ALL_QUESTIONS);
        try {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Question question = new Question();
                question.setQuestionID(rs.getInt(1));
                question.setQuestion(rs.getString(2));
                question.setTestID(rs.getInt(3));
                questions.add(question);
            }
        } finally {
            closePrepareStatement(ps);
        }
        return questions;
    }

    @Override
    public Question update(Question entity) throws SQLException {
        String UPDATE_QUESTION = "update questions set question = ?, testID = ? where questionID = ?";
        PreparedStatement ps = getPrepareStatement(UPDATE_QUESTION);
        try {
            ps.setString(1, entity.getQuestion());
            ps.setInt(2, entity.getTestID());
            ps.setInt(3, entity.getQuestionID());
            ps.executeUpdate();
        } finally {
            closePrepareStatement(ps);
        }
        return entity;
    }

    @Override
    public Question getEntityById(Integer id) throws SQLException {
        String SELECT_QUESTION = "select * from questions where questionID = ?";
        PreparedStatement ps = getPrepareStatement(SELECT_QUESTION);
        Question question = new Question();
        try {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                question.setQuestionID(rs.getInt(1));
                question.setQuestion(rs.getString(2));
                question.setTestID(rs.getInt(3));
            }
        } finally {
            closePrepareStatement(ps);
        }
        return question;
    }

    @Override
    public boolean delete(Integer id) throws SQLException {
        String DELETE_QUESTION = "delete from questions where questionID = ?";
        PreparedStatement ps = getPrepareStatement(DELETE_QUESTION);
        try {
            ps.setInt(1, id);
            ps.executeUpdate();
        } finally {
            closePrepareStatement(ps);
        }
        return true;
    }

    @Override
    public boolean create(Question entity) throws SQLException {
        String CREATE_QUESTION = "insert into questions (question, testID) values (?, ?)";
        PreparedStatement ps = getPrepareStatement(CREATE_QUESTION);
        try {
            ps.setString(1, entity.getQuestion());
            ps.setInt(2, entity.getTestID());
            ps.executeUpdate();
        } finally {
            closePrepareStatement(ps);
        }
        return true;
    }

    public ArrayList<Question> getQuestionsByTestId(Integer testID) throws SQLException {
        String SELECT_QUESTIONS_BY_TESTID = "select * from questions where testID = ?";
        PreparedStatement ps = getPrepareStatement(SELECT_QUESTIONS_BY_TESTID);
        ArrayList<Question> questions = new ArrayList<>();
        try {
            ps.setInt(1, testID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Question question = new Question();
                question.setQuestionID(rs.getInt(1));
                question.setQuestion(rs.getString(2));
                question.setTestID(testID);
                questions.add(question);
            }
        } finally {
            closePrepareStatement(ps);
        }
        return questions;
    }
}
