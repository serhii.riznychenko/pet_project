<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><i18n:localization message="my_account"/></title>
    <link href="<c:url value='/css/main.css'/>" rel="stylesheet">
    <jsp:include page="_header.jsp"></jsp:include>
</head>
<body>
<jsp:include page="/${loggedUser.role.name().toLowerCase()}/_menu.jsp"></jsp:include>
<br>
<div style="text-align: center; margin: 0 auto; border: 3px solid #00905d;width:400px;padding:10px;">
    <i18n:localization message="user_login"/>: ${loggedUser.login}
    <br>
    <i18n:localization message="email"/>: ${loggedUser.email}
    <br>
    <i18n:localization message="user_name"/>: ${loggedUser.realName}
    <br/>
    <i18n:localization message="age"/>: ${loggedUser.age}
    <br>
</div>
<br>
<a class="button" href="${pageContext.request.contextPath}/profile/update"><i18n:localization message="updating_profile"/></a>
<br>
<jsp:include page="_footer.jsp"></jsp:include>
</body>
</html>
