<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><i18n:localization message="user_registration"/></title>
    <jsp:include page="_header.jsp"></jsp:include>
    <link href="css/registration.css" rel="stylesheet">
</head>
<body>
<div class="registration-page">
    <div class="form">
        <form class="register-form" method="post" action="registration" accept-charset="UTF-8">
            <p style="color: #EF3B3A;"><i18n:localization message="${error}"/></p>
            <input type="text" name="login" placeholder="<i18n:localization message="user_login"/>" value="" required/>
            <input type="password" name="password" placeholder="<i18n:localization message="password"/>" value="" required/>
            <input type="text" placeholder="<i18n:localization message="email"/>" name="email" value="" required/>
            <input type="text" name="userName" placeholder="<i18n:localization message="user_name"/>" value="" required/>
            <input type="text" name="age" placeholder="<i18n:localization message="age"/>" value="" required/>
            <button type="submit"><i18n:localization message="submit"/></button>
            <p class="message"><i18n:localization message="already_registered?"/><a href="${pageContext.request.contextPath}/login"><i18n:localization message="login"/></a></p>
        </form>
    </div>
</div>
<br>
<jsp:include page="_footer.jsp"></jsp:include>
</body>
</html>
