<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<link href="<c:url value='/css/main.css'/>" rel="stylesheet">
<div class="footer">
    <div class="language">
        <i18n:localization message="сhange_language"/>
        <form>
            <a href="${pageContext.request.contextPath}/locale-change?locale=en">EN</a>
            |
            <a href="${pageContext.request.contextPath}/locale-change?locale=ru">RU</a>
            |
            <a href="${pageContext.request.contextPath}/locale-change?locale=ua">UA</a>
        </form>
    </div>
    <div class="copyright">
        <i18n:localization message="copyright"/>
    </div>
</div>