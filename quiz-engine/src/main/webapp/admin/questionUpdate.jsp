<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><i18n:localization message="changing_a_question"/></title>
    <link href="<c:url value='/admin/css/main.css'/>" rel="stylesheet">
    <jsp:include page="../_header.jsp"></jsp:include>
</head>
<body>
<jsp:include page="_menu.jsp"></jsp:include>
<br>
<a class="button" href="${pageContext.request.contextPath}/admin/tests/update?testID=${question.testID}"><i18n:localization message="go_back"/></a>
<br>
<div style="text-align: center; margin: 0 auto; border: 3px solid #00905d;width:1000px;padding:10px;">
    <form method="post" action="${pageContext.request.contextPath}/admin/tests/questions/update"
          accept-charset="UTF-8">
        <i18n:localization message="question_id"/>: <input type="text" name="questionID" value="${question.questionID}" readonly>
        <br>
        <i18n:localization message="test_id"/>: <input type="text" name="testID" value="${question.testID}" readonly>
        <br>
        <h4><i18n:localization message="question"/>:</h4>
        <br>
        <textarea name="question" required>${question.question}</textarea>
        <br>
        <hr>
        <input type="submit" value="<i18n:localization message="submit"/>">
    </form>
</div>
<br>
<a class="button"
   href="${pageContext.request.contextPath}/admin/tests/questions/answers/create?testID=${question.testID}&questionID=${question.questionID}"><i18n:localization message="сreate_answer"/></a>
<br>
<center>
    <h3><i18n:localization message="answers"/>:</h3>
    <c:forEach var="answer" items="${answers}">
        <table>
            <thead>
            <tr>
                <th><i18n:localization message="answer_id"/>: ${answer.answerID}</th>
                <th><i18n:localization message="is_correct"/>: ${answer.isCorrect()}</th>
                <th><i18n:localization message="question_id"/>: ${answer.questionID}</th>
                <th>
                    <form action="${pageContext.request.contextPath}/admin/tests/questions/answers/update" method="get">
                        <input type="hidden" name="testID" value="${question.testID}" required>
                        <input type="hidden" name="answerID" value="${answer.answerID}">
                        <button type="submit" class="btn-link"><i18n:localization message="change"/></button>
                    </form>
                </th>
                <th>
                    <form action="${pageContext.request.contextPath}/admin/tests/questions/answers/delete"
                          method="post">
                        <input type="hidden" name="questionID" value="${question.questionID}" required>
                        <input type="hidden" name="answerID" value="${answer.answerID}">
                        <button type="submit" class="btn-link"><i18n:localization message="delete"/></button>
                    </form>
                </th>
            </tr>
            </thead>
        </table>
        <textarea name="answer" readonly>${answer.answer}</textarea>
        <br>
    </c:forEach>
</center>
<br>
<jsp:include page="../_footer.jsp"></jsp:include>
</body>
</html>
