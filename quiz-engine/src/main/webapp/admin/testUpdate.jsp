<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><i18n:localization message="test_updating"/></title>
    <link href="<c:url value='/admin/css/main.css'/>" rel="stylesheet">
    <jsp:include page="../_header.jsp"></jsp:include>
</head>
<body>
<jsp:include page="_menu.jsp"></jsp:include>
<br>
<a class="button" href="${pageContext.request.contextPath}/admin/tests"><i18n:localization message="go_back"/></a>
<br>
<br>
<div style="text-align: right; margin: 0 auto; border: 3px solid #00905d;width:500px;padding:10px;">
    <form method="post" action="${pageContext.request.contextPath}/admin/tests/update" accept-charset="UTF-8">
        <i18n:localization message="test_id"/>: <input type="text" name="testID" value="${test.testID}" readonly>
        <br>
        <i18n:localization message="test_name"/>: <input type="text" name="name" value="${test.name}" required>
        <br>
        <i18n:localization message="hard_level"/>: <input type="text" name="hardLevel" value="${test.hardLevel}" required>
        <br>
        <i18n:localization message="topic_name"/>: <input type="text" name="topic" value="${test.topic}" required>
        <br>
        <i18n:localization message="time_to_solve"/>: <input type="text" name="timeToSolve" value="${test.timeToSolve}" required>
        <br>
        <hr>
        <center>
            <button type="submit" class="btn-link"><i18n:localization message="submit"/></button>
        </center>
    </form>
</div>
<br>
<a class="button" href="${pageContext.request.contextPath}/admin/tests/questions/create?testID=${test.testID}"><i18n:localization message="create_question"/></a>
<br>
<center>
    <h3><i18n:localization message="questions"/>:</h3>
    <c:forEach var="question" items="${questions}">
        <table>
            <thead>
            <tr>
                <th><i18n:localization message="question_id"/>: ${question.questionID}</th>
                <th>
                    <form action="${pageContext.request.contextPath}/admin/tests/questions/update" method="get">
                        <input type="hidden" name="testID" value="${test.testID}" required>
                        <input type="hidden" name="questionID" value="${question.questionID}">
                        <button type="submit" class="btn-link"><i18n:localization message="change"/></button>
                    </form>
                </th>
                <th>
                    <form action="${pageContext.request.contextPath}/admin/tests/questions/delete" method="post">
                        <input type="hidden" name="testID" value="${test.testID}" required>
                        <input type="hidden" name="questionID" value="${question.questionID}">
                        <button type="submit" class="btn-link"><i18n:localization message="delete"/></button>
                    </form>
                </th>
            </tr>
            </thead>
        </table>
        <textarea name="question" readonly>${question.question}</textarea>
        <br>
    </c:forEach>
</center>
<br>
<jsp:include page="../_footer.jsp"></jsp:include>
</body>
</html>
