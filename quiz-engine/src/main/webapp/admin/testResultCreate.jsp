<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><i18n:localization message="test_result_creating"/></title>
    <link href="<c:url value='/admin/css/main.css'/>" rel="stylesheet">
    <jsp:include page="../_header.jsp"></jsp:include>
</head>
<body>
<jsp:include page="_menu.jsp"></jsp:include>
<br>
<a class="button" href="${pageContext.request.contextPath}/admin/results"><i18n:localization message="go_back"/></a>
<br>
<div style="text-align: center; margin: 0 auto; border: 3px solid #00905d;width:400px;padding:10px;">
    <form method="post" action="${pageContext.request.contextPath}/admin/results/create">
            <input type="text" name="userID" placeholder="<i18n:localization message="user_id"/>" value="" required>
            <br>
            <input type="text" name="testID" placeholder="<i18n:localization message="test_id"/>" value="" required>
            <br>
            <hr>
            <button type="submit" class="btn-link"><i18n:localization message="submit"/></button>
    </form>
</div>
<br>
<br>
<jsp:include page="../_footer.jsp"></jsp:include>
</body>
</html>
