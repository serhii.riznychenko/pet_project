<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><i18n:localization message="test_list"/></title>
    <link href="<c:url value='/admin/css/main.css'/>" rel="stylesheet">
    <jsp:include page="../_header.jsp"></jsp:include>
</head>
<body>
<jsp:include page="_menu.jsp"></jsp:include>
<br>
<a class="button" href="/quiz-engine/admin/tests/create"><i18n:localization message="create_test"/></a>
<br>
<center>
    <table>
        <thead>
        <tr>
            <th><i18n:localization message="test_id"/></th>
            <th><i18n:localization message="test_name"/></th>
            <th><i18n:localization message="hard_level"/></th>
            <th><i18n:localization message="topic_name"/></th>
            <th><i18n:localization message="time_to_solve"/></th>
            <th><i18n:localization message="change_test"/></th>
            <th><i18n:localization message="delete_test"/></th>
        </tr>
        </thead>
        <c:forEach var="testsResult" items="${tests}">
            <tbody>
            <tr>
                <th>${testsResult.testID}</th>
                <th>${testsResult.name}</th>
                <th>${testsResult.hardLevel}</th>
                <th>${testsResult.topic}</th>
                <th>${testsResult.timeToSolve}</th>
                <th>
                    <form action="${pageContext.request.contextPath}/admin/tests/update" method="get">
                        <input type="hidden" name="testID" value="${testsResult.testID}">
                        <button type="submit" class="btn-link"><i18n:localization message="change"/></button>
                    </form>
                </th>
                <th>
                    <form action="${pageContext.request.contextPath}/admin/tests" method="post">
                        <input type="hidden" name="testID" value="${testsResult.testID}">
                        <button type="submit" class="btn-link"><i18n:localization message="delete"/></button>
                    </form>
                </th>
            </tr>
            </tbody>
        </c:forEach>
    </table>
</center>
<br>
<jsp:include page="../_footer.jsp"></jsp:include>
</body>
</html>
