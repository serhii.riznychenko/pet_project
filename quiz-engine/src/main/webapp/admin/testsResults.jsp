<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><i18n:localization message="test_results"/></title>
    <link href="<c:url value='/admin/css/main.css'/>" rel="stylesheet">
    <jsp:include page="../_header.jsp"></jsp:include>
</head>
<body>
<jsp:include page="_menu.jsp"></jsp:include>
<br>
<a class="button" href="${pageContext.request.contextPath}/admin/results/create"><i18n:localization message="create_test_result"/></a>
<br>
<center>
    <table>
        <thead>
        <tr>
            <th><i18n:localization message="result_id"/></th>
            <th><i18n:localization message="user_id"/></th>
            <th><i18n:localization message="test_id"/></th>
            <th><i18n:localization message="test_name"/></th>
            <th><i18n:localization message="topic_name"/></th>
            <th><i18n:localization message="hard_level"/></th>
            <th><i18n:localization message="time_to_solve"/></th>
            <th><i18n:localization message="is_done"/></th>
            <th><i18n:localization message="points"/></th>
            <th><i18n:localization message="delete_result"/></th>
        </tr>
        </thead>
        <c:forEach var="testsResult" items="${resultList}">
            <tbody>
            <tr>
                <th>${testsResult.resultID}</th>
                <th>${testsResult.userID}</th>
                <th>${testsResult.testID}</th>
                <th>${testsResult.testModule.name}</th>
                <th>${testsResult.testModule.topic}</th>
                <th>${testsResult.testModule.hardLevel}</th>
                <th>${testsResult.testModule.timeToSolve}</th>
                <th>${testsResult.isDone}</th>
                <th>${testsResult.points}</th>
                <th>
                    <form action="${pageContext.request.contextPath}/admin/results" method="post">
                        <input type="hidden" name="resultID" value="${testsResult.resultID}">
                        <button type="submit" class="btn-link"><i18n:localization message="delete"/></button>
                    </form>
                </th>
            </tr>
            </tbody>
        </c:forEach>
    </table>
</center>
<br>
<jsp:include page="../_footer.jsp"></jsp:include>
</body>
</html>
