<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><i18n:localization message="creating_question"/></title>
    <link href="<c:url value='/admin/css/main.css'/>" rel="stylesheet">
    <jsp:include page="../_header.jsp"></jsp:include>
</head>
<body>
<jsp:include page="_menu.jsp"></jsp:include>
<br>
<a class="button" href="${pageContext.request.contextPath}/admin/tests/update?testID=${param.testID}"><i18n:localization message="go_back"/></a>
<br>
<div style="text-align: center; margin: 0 auto; border: 3px solid #00905d;width:1000px;padding:10px;">
    <form method="post" action="${pageContext.request.contextPath}/admin/tests/questions/create"
          accept-charset="UTF-8">
        <br>
        <i18n:localization message="test_id"/>: <input type="text" name="testID" value="${param.testID}" readonly>
        <br>
        <h4><i18n:localization message="question"/>:</h4>
        <textarea name="question" required></textarea>
        <hr>
        <input type="submit" value="<i18n:localization message="submit"/>">
    </form>
</div>
<br>
<jsp:include page="../_footer.jsp"></jsp:include>
</body>
</html>


