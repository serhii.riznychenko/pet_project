<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><i18n:localization message="create_test"/></title>
    <link href="<c:url value='/admin/css/main.css'/>" rel="stylesheet">
    <jsp:include page="../_header.jsp"></jsp:include>
</head>
<body>
<jsp:include page="_menu.jsp"></jsp:include>
<br>
<a class="button" href="${pageContext.request.contextPath}/admin/tests"><i18n:localization message="go_back"/></a>
<br>
<div style="text-align: center; margin: 0 auto; border: 3px solid #00905d;width:600px;padding:10px;">
    <form method="post" action="${pageContext.request.contextPath}/admin/tests/create">
        <input type="text" name="name" placeholder="<i18n:localization message="test_name"/>" value="" required>
        <br>
        <input type="text" name="hardLevel" placeholder="<i18n:localization message="hard_level"/>" value="" required>
        <br>
        <input type="text" name="topic" placeholder="<i18n:localization message="topic_name"/>" value="" required>
        <br/>
        <input type="text" name="timeToSolve" placeholder="<i18n:localization message="time_to_solve"/>" value="" required>
        <br>
        <hr>
        <button type="submit" class="btn-link"><i18n:localization message="submit"/></button>
    </form>
</div>
<br>
<jsp:include page="../_footer.jsp"></jsp:include>
</body>
</html>