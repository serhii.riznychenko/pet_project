<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<link href="<c:url value='/admin/css/main.css'/>" rel="stylesheet">
<div style="padding: 5px;">
    <a class="button" href="${pageContext.request.contextPath}/admin"><i18n:localization message="home_page"/></a>
    <a class="button" href="${pageContext.request.contextPath}/admin/tests"><i18n:localization message="test_list"/></a>
    <a class="button" href="${pageContext.request.contextPath}/admin/results"><i18n:localization message="test_results"/></a>
</div>