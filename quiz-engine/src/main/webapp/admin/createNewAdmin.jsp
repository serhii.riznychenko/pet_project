<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><i18n:localization message="сreating_new_admin"/></title>
    <jsp:include page="../_header.jsp"></jsp:include>
    <link href="<c:url value='/admin/css/main.css'/>" rel="stylesheet">
</head>
<body>
<jsp:include page="_menu.jsp"></jsp:include>
<br>
<div style="text-align: right; margin: 0 auto; border: 3px solid #00905d;width:400px;padding:10px;">
    <center>
        <p style="color: #EF3B3A;"><i18n:localization message="${error}"/></p>
    </center>
    <form method="post" action="${pageContext.request.contextPath}/admin/create">
        <i18n:localization message="user_id"/>: <input type="text" name="userID" value="" required>
        <br>
        <i18n:localization message="user_login"/>: <input type="text" name="login" value="" required>
        <br>
        <hr>
        <center>
            <button type="submit" class="btn-link"><i18n:localization message="submit"/></button>
        </center>
    </form>
</div>
<br>
<jsp:include page="../_footer.jsp"></jsp:include>
</body>
</html>
