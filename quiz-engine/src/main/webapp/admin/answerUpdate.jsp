<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><i18n:localization message="updating_answer"/></title>
    <link href="<c:url value='/admin/css/main.css'/>" rel="stylesheet">
    <jsp:include page="../_header.jsp"></jsp:include>
</head>
<body>
<jsp:include page="_menu.jsp"></jsp:include>
<br>
<a class="button" href="${pageContext.request.contextPath}/admin/tests/questions/update?testID=${param.testID}&questionID=${answer.questionID}"><i18n:localization message="go_back"/></a>
<br>
<div style="text-align: center; margin: 0 auto; border: 3px solid #00905d;width:1000px;padding:10px;">
    <form method="post" action="${pageContext.request.contextPath}/admin/tests/questions/answers/update">
        <input type="hidden" name="testID" value="${param.testID}">
        <i18n:localization message="question_id"/>: <input type="text" name="questionID" value="${answer.questionID}" readonly>
        <br>
        <i18n:localization message="answer_id"/>: <input type="text" name="answerID" value="${answer.answerID}" readonly>
        <br>
        <h4><i18n:localization message="answer"/>:</h4>
        <br>
        <textarea name="answer">${answer.answer}</textarea>
        <br>
        <i18n:localization message="is_correct"/>: <input type="checkbox" name="isCorrect" value="true">
        <br>
        <hr>
        <input type="submit" value="<i18n:localization message="submit"/>">
    </form>
</div>
<br>
<jsp:include page="../_footer.jsp"></jsp:include>
</body>
</html>


