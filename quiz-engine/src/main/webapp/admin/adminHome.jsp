<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><i18n:localization message="home_page"/></title>
    <jsp:include page="../_header.jsp"></jsp:include>
    <link href="<c:url value='/admin/css/main.css'/>" rel="stylesheet">
</head>
<body>
<jsp:include page="_menu.jsp"></jsp:include>
<br>
<center>
    <p style="color: #EF3B3A;"><i18n:localization message="${error}"/></p>
    <table>
        <thead>
        <tr>
            <th><i18n:localization message="user_id"/></th>
            <th><i18n:localization message="user_login"/></th>
            <th><i18n:localization message="email"/></th>
            <th><i18n:localization message="user_name"/></th>
            <th><i18n:localization message="ban_status"/></th>
            <th><i18n:localization message="change_ban_status"/></th>
        </tr>
        </thead>
        <c:forEach var="user" items="${users}">
            <tbody>
            <tr>
                <th>${user.userID}</th>
                <th>${user.login}</th>
                <th>${user.email}</th>
                <th>${user.realName}</th>
                <th>${user.isBanned}</th>
                <th>
                    <form action="${pageContext.request.contextPath}/admin/ban" method="post">
                        <input type="hidden" name="userID" value="${user.userID}">
                        <button type="submit" class="btn"><i18n:localization message="сhange"/></button>
                    </form>
                </th>
            </tr>
            </tbody>
        </c:forEach>
    </table>
</center>
<a class="button" href="${pageContext.request.contextPath}/admin/create"><i18n:localization message="create_new_admin"/></a>
<br>
<jsp:include page="../_footer.jsp"></jsp:include>
</body>
</html>
