<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><i18n:localization message="test_list"/></title>
    <link href="<c:url value='/user/css/main.css'/>" rel="stylesheet">
    <jsp:include page="../_header.jsp"></jsp:include>
</head>
<body>
<jsp:include page="_menu.jsp"></jsp:include>
<br>
<div class="sort">
    <div class="class-chose">
        <form method="get" action="${pageContext.request.contextPath}/home/tests">
            <p><b><i18n:localization message="which_topic_to_choose"/>?</b></p>
            <input type="text" name="topic" value="">
            <input type="submit" value="<i18n:localization message="submit"/>">
        </form>
    </div>
    <div class="test-sort">
        <form method="get" action="${pageContext.request.contextPath}/home/tests/sorting">
            <p><b><i18n:localization message="how_to_sort_tests"/>?</b></p>
            <select name="sort" size="1">
                <option value="name"><i18n:localization message="test_name"/></option>
                <option value="topic"><i18n:localization message="topic_name"/></option>
                <option value="questions"><i18n:localization message="amount_of_questions"/></option>
                <option value="hard"><i18n:localization message="hard_level"/></option>
            </select>
            <input type="hidden" name="url" value="${pageContext.request.contextPath}/home/tests">
            <input type="submit" value="<i18n:localization message="submit"/>">
        </form>
        <br>
    </div>
</div>
<br>
<br>
<center>
    <c:forEach var="testsResultEntry" items="${testsResultMap.entrySet()}">
    <table>
        <thead>
        <tr>
            <th><i18n:localization message="test_name"/></th>
            <th><i18n:localization message="topic_name"/></th>
            <th><i18n:localization message="hard_level"/></th>
            <th><i18n:localization message="time_to_solve"/></th>
            <th><i18n:localization message="amount_of_questions"/></th>
            <th><i18n:localization message="testing"/></th>
        </tr>
        </thead>
        <c:forEach var="testsResult" items="${testsResultEntry.getValue()}">
            <tbody>
            <tr>
                <th>${testsResult.testModule.name}</th>
                <th>${testsResult.testModule.topic}</th>
                <th>${testsResult.testModule.hardLevel}</th>
                <th>${testsResult.testModule.timeToSolve}</th>
                <th>${testsResult.testModule.questions.size()}</th>
                <th>
                    <a href="${pageContext.request.contextPath}/testing?testResultID=${testsResult.resultID}"><i18n:localization message="start"/></a>
                </th>
            </tr>
            </tbody>
        </c:forEach>
    </table>
    <br>
    </c:forEach>
    <center>
        <br>
        <jsp:include page="../_footer.jsp"></jsp:include>
</body>
</html>
