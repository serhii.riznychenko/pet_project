<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<div style="padding: 5px;">
    <a class="button" href="${pageContext.request.contextPath}/home"><i18n:localization message="home_page"/></a>
    <a class="button" href="${pageContext.request.contextPath}/home/tests"><i18n:localization message="test_list"/></a>
</div>
