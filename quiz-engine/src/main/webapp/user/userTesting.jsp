<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><i18n:localization message="testing"/></title>
    <link href="<c:url value='/user/css/main.css'/>" rel="stylesheet">
    <jsp:include page="../_header.jsp"></jsp:include>
</head>
<body>
<br>
<center>
    <h2><i18n:localization message="timer"/>: <span id="counter"></span></h2>
</center>
<form action="${pageContext.request.contextPath}/testing" method="post">
    <c:forEach var="Question" items="${testModule.questions}">
        ${Question.question}
        <c:forEach var="Answer" items="${Question.answers}">
            <p><input name="answer" type="checkbox" value="${Answer.answerID}">${Answer.answer}</p>
            <br>
        </c:forEach>
        <hr>
        <br>
    </c:forEach>
    <button type="submit" class="btn-link"><i18n:localization message="end_test"/></button>
</form>
<br>
<jsp:include page="../_footer.jsp"></jsp:include>
</body>

<script>
    window.onload = function countdown() {
        var seconds = 60;
        var mins = ${testModule.timeToSolve};

        function tick() {
            var counter = document.getElementById("counter");
            var current_minutes = mins - 1;
            seconds--;
            counter.innerHTML = current_minutes.toString() + ":" + (seconds < 10 ? "0" : "") + String(seconds);
            if (seconds > 0) {
                setTimeout(tick, 1000);
            } else {
                if (mins > 1) {
                    countdown(mins - 1);
                } else {
                    document.forms[0].submit();
                }
            }
        }
        tick();
    }
</script>
</html>
