<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<html>
<head>
    <title><i18n:localization message="404_title"/></title>
    <link href="<c:url value='/admin/css/main.css'/>" rel="stylesheet">
    <jsp:include page="../_header.jsp"></jsp:include>
</head>
<body>
<center>
    <h1>
        <i18n:localization message="404_message"/>
    </h1>
</center>
<br>
<jsp:include page="../_footer.jsp"></jsp:include>
</body>
</html>
