<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>
        <i18n:localization message="index_page"/>
    </title>
    <jsp:include page="_header.jsp"></jsp:include>
    <link href="css/main.css" rel="stylesheet">
</head>
<body>
<br>
<a class="button" href="${pageContext.request.contextPath}/registration"><i18n:localization
        message="register_now"/>!</a>
<a class="button" href="${pageContext.request.contextPath}/login"><i18n:localization message="login"/>!</a>
<br>
<br>
<center>
    <h2><i18n:localization message="index_advice"/></h2>
    <br>
</center>
<br>
<jsp:include page="_footer.jsp"></jsp:include>
</body>
</html>
