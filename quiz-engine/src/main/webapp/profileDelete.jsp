<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><i18n:localization message="deleting_profile"/></title>
    <link href="<c:url value='/css/main.css'/>" rel="stylesheet">
    <jsp:include page="_header.jsp"></jsp:include>
</head>
<body>
<jsp:include page="/${loggedUser.role.name().toLowerCase()}/_menu.jsp"></jsp:include>
<br>
<center>
    <h3><i18n:localization message="profile_delete_question"/></h3>
</center>
<div style="text-align: center; margin: 0 auto; border: 3px solid #00905d;width:150px;padding:10px;">
    <form style="display: inline" method="post" action="${pageContext.request.contextPath}/profile/delete">
        <button type="submit" class="btn-link"><i18n:localization message="yes"/></button>
    </form>
    <form style="display: inline" method="get" action="${pageContext.request.contextPath}/profile">
        <button type="submit" class="btn-link"><i18n:localization message="no"/></button>
    </form>
</div>
<br>
<jsp:include page="_footer.jsp"></jsp:include>
</body>
</html>
