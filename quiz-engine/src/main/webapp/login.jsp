<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="css/login.css" rel="stylesheet">
    <title><i18n:localization message="login_page"/></title>
    <jsp:include page="_header.jsp"></jsp:include>
</head>
<body>
<br>
<div class="login-page">
    <div class="form">
        <form class="login-form" method="post" action="login" accept-charset="UTF-8">
            <p style="color: #EF3B3A;"><i18n:localization message="${error}"/></p>
            <input type="text" name="login" placeholder="<i18n:localization message="user_login"/>" value="" required>
            <input type="password" name="password" placeholder="<i18n:localization message="password"/>" value="" required>
            <button type="submit"><i18n:localization message="login"/></button>
            <p class="message"><i18n:localization message="not_registered?"/>  <a href="${pageContext.request.contextPath}/registration"><i18n:localization message="register_now"/></a></p>
        </form>
    </div>
</div>
<br>
<jsp:include page="_footer.jsp"></jsp:include>
</body>
</html>
