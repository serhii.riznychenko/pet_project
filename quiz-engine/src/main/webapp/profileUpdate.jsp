<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><i18n:localization message="updating_profile"/></title>
    <link href="<c:url value='/css/main.css'/>" rel="stylesheet">
    <jsp:include page="_header.jsp"></jsp:include>
</head>
<body>
<jsp:include page="/${loggedUser.role.name().toLowerCase()}/_menu.jsp"></jsp:include>
<br>
<div style="text-align: right; margin: 0 auto; border: 3px solid #00905d;width:400px;padding:10px;">
    <center>
        <p style="color: #EF3B3A;"><i18n:localization message="${error}"/></p>
    </center>
    <form method="post" action="${pageContext.request.contextPath}/profile/update">
        <i18n:localization message="user_login"/>: <input type="text" name="login" placeholder="" value="${loggedUser.login}">
        <br>
        <i18n:localization message="email"/>: <input type="text" name="email" value="${loggedUser.email}">
        <br>
        <i18n:localization message="user_name"/>: <input type="text" name="realName" value="${loggedUser.realName}">
        <br>
        <i18n:localization message="age"/>: <input type="text" name="age" value="${loggedUser.age}">
        <br/>
        <hr>
        <center>
            <button type="submit" class="btn-link"><i18n:localization message="submit"/></button>
        </center>
    </form>
</div>
<br>
<a class="button" href="${pageContext.request.contextPath}/account/password/update"><i18n:localization message="password_change"/></a>
<a class="button" href="${pageContext.request.contextPath}/profile/delete"><i18n:localization message="deleting_profile"/></a>
<br>
<jsp:include page="_footer.jsp"></jsp:include>
</body>
</html>
