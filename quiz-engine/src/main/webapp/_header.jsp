<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tag/localization.tld" prefix="i18n" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link href="<c:url value='/css/main.css'/>" rel="stylesheet">
<header>
    <link rel="icon" href="<c:url value='/img/icon.ico'/>">
    <div class="header">
        <div class="logo">
            <a href="${pageContext.request.contextPath}"><img src="<c:url value='/img/logo.png'/>" height="100"
                                                              alt="Quiz engine"></a>
        </div>
        <div class="user-info">
            <c:if test="${loggedUser != null}">
                <i18n:localization message="hello"/>, <b>${loggedUser.login}</b>
                <br>
                <a class="button" href="${pageContext.request.contextPath}/profile"><i18n:localization message="my_profile"/></a>
                <a class="button" href="${pageContext.request.contextPath}/logout"><i18n:localization message="logout"/></a>
            </c:if>
        </div>
    </div>
</header>
