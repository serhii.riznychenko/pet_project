package com.epam.projects.pet.services;

import com.epam.projects.pet.dao.UserDao;
import com.epam.projects.pet.models.User;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.exception.LockException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import util.H2Connector;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class AccountServiceTest {
    private H2Connector h2Connector = new H2Connector();
    private UserDao userDao;
    private AccountService accountService;
    private User sut;

    @BeforeEach
    void init() throws SQLException, ClassNotFoundException, LiquibaseException {
        h2Connector.buildDb();
        userDao = new UserDao();
        accountService = new AccountService();
        sut = new User();
        sut.setLogin("John");
        sut.setEmail("test@i.com");
        sut.setPassword("$2a$12$3jqQj2zdM/z5TX5Q31Foh.erK9Qc.gyh9DtlwGB1TffMc/vIBqkiS".toCharArray()); //hash of 12345
        sut.setRealName("Smith");
        sut.setAge(22);
    }

    @AfterEach
    void drop() throws DatabaseException, LockException, SQLException {
        h2Connector.dropDb();
    }

    @Test
    void createUserAccount() throws SQLException {
        User user = accountService.createUserAccount("test", "test@test.com",
                "test", "12345", "21", User.Role.USER);
        assertEquals("test", user.getLogin());
        assertEquals("test", user.getRealName());
        assertEquals("test@test.com", user.getEmail());
    }

    @Test
    void getUserAccount() throws SQLException, IllegalAccessException {
        userDao.create(sut);
        User user = accountService.getUserAccount("John", "12345");
        assertEquals("John", user.getLogin());
        assertEquals("Smith", user.getRealName());
        assertEquals("test@i.com", user.getEmail());
    }

    @Test
    void updateUserPassword() throws SQLException {
        userDao.create(sut);
        accountService.updateUserPassword("12345", "111", sut);
        User sut2 = userDao.getEntityById(sut.getUserID());
        assertNotEquals(String.valueOf(sut.getPassword()), String.valueOf(sut2.getPassword()));

    }

    @Test
    void checkUserBanStatus() throws SQLException {
        userDao.create(sut);
        assertEquals(false, accountService.checkUserBanStatus(sut));
    }
}