package com.epam.projects.pet.services;

import com.epam.projects.pet.dao.AnswerDao;
import com.epam.projects.pet.dao.QuestionDao;
import com.epam.projects.pet.dao.TestModuleDao;
import com.epam.projects.pet.dao.UserDao;
import com.epam.projects.pet.models.Answer;
import com.epam.projects.pet.models.Question;
import com.epam.projects.pet.models.TestModule;
import com.epam.projects.pet.models.User;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.exception.LockException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import util.H2Connector;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AnswerServiceTest {
    private H2Connector h2Connector = new H2Connector();
    private AnswerDao answerDao;
    private AnswerService answerService;
    private Answer sut;

    @BeforeEach
    void init() throws SQLException, ClassNotFoundException, LiquibaseException {
        h2Connector.buildDb();
        TestModuleDao testModuleDao = new TestModuleDao();
        TestModule testModule = new TestModule();
        testModule.setTestID(1);
        testModule.setName("Java");
        testModule.setHardLevel(3);
        testModule.setTopic("Beginning");
        testModule.setTimeToSolve(15);
        testModuleDao.create(testModule);

        QuestionDao questionDao = new QuestionDao();
        Question question = new Question();
        question.setQuestionID(1);
        question.setQuestion("Something");
        question.setTestID(1);
        questionDao.create(question);

        answerService = new AnswerService();
        answerDao = new AnswerDao();
        sut = new Answer();
        sut.setAnswer("Something");
        sut.setCorrect(true);
        sut.setQuestionID(1);
    }

    @AfterEach
    void drop() throws DatabaseException, LockException, SQLException {
        h2Connector.dropDb();
    }

    @Test
    void createAnswer() throws SQLException {
        Answer answer = answerService.createAnswer("Something", true, 1);
        assertEquals("Something", answer.getAnswer());
        assertEquals(true, answer.isCorrect());
    }

    @Test
    void getAnswer() throws SQLException {
        answerDao.create(sut);
        Answer answer = answerService.getAnswer(1);
        assertEquals("Something", answer.getAnswer());
        assertEquals(true, answer.isCorrect());
    }

    @Test
    void getAnswersByQuestionID() throws SQLException {
        answerDao.create(sut);
        List<Answer> answers = answerService.getAnswersByQuestionID(1);
        assertEquals(1, answers.size());
    }

    @Test
    void updateAnswer() throws SQLException {
        answerDao.create(sut);
        Answer answer = answerService.updateAnswer(1, "Test", false, 1);
        assertEquals("Test", answer.getAnswer());
        assertEquals(false, answer.isCorrect());
    }

    @Test
    void deleteAnswerByID() throws SQLException {
        answerDao.create(sut);
        answerService.deleteAnswerByID(1);
        assertEquals(null, answerDao.getEntityById(1).getAnswer());
    }
}