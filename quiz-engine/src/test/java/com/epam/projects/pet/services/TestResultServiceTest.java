package com.epam.projects.pet.services;

import com.epam.projects.pet.dao.TestResultDao;
import com.epam.projects.pet.models.TestResult;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.exception.LockException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import util.H2Connector;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TestResultServiceTest {
    private H2Connector h2Connector = new H2Connector();
    private TestResultService testResultService;
    private TestResultDao testResultDao;
    private TestResult sut;

    @BeforeEach
    void init() throws SQLException, ClassNotFoundException, LiquibaseException {
        h2Connector.buildDb();
        testResultService = new TestResultService();
        testResultDao = new TestResultDao();
        sut = new TestResult();
        sut.setUserID(1);
        sut.setTestID(1);
        sut.setIsDone(true);
        sut.setPoints(10);
    }

    @AfterEach
    void drop() throws DatabaseException, LockException, SQLException {
        h2Connector.dropDb();
    }

    @Test
    void createTestResult() throws SQLException {
        testResultService.createTestResult(1, 1);
        assertEquals(1, testResultDao.getEntityById(1).getUserID());
        assertEquals(1, testResultDao.getEntityById(1).getTestID());
    }

    @Test
    void getTestResult() throws SQLException {
        testResultDao.create(sut);
        TestResult testResult = testResultService.getTestResult(1);
        assertEquals(1, testResult.getUserID());
        assertEquals(1, testResult.getTestID());
    }

    @Test
    void updateTestResult() throws SQLException {
        testResultDao.create(sut);
        testResultService.updateTestResult(1,20);
        assertEquals(20, testResultDao.getEntityById(1).getPoints());
    }

    @Test
    void getAllTestResults() throws SQLException {
        testResultDao.create(sut);
        List<TestResult> testResults = testResultService.getAllTestResults();
        assertEquals(1, testResults.size());
    }

    @Test
    void deleteTestResult() throws SQLException {
        testResultDao.create(sut);
        testResultService.deleteTestResult(1);
        assertEquals(0, testResultDao.getEntityById(1).getPoints());
        assertEquals(false, testResultDao.getEntityById(1).getIsDone());
    }
}