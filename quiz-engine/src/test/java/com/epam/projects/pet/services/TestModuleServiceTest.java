package com.epam.projects.pet.services;

import com.epam.projects.pet.dao.TestModuleDao;
import com.epam.projects.pet.models.TestModule;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.exception.LockException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import util.H2Connector;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TestModuleServiceTest {
    private H2Connector h2Connector = new H2Connector();
    private TestModuleService testModuleService;
    private TestModuleDao testModuleDao;
    private TestModule sut;

    @BeforeEach
    void init() throws SQLException, ClassNotFoundException, LiquibaseException {
        h2Connector.buildDb();
        testModuleService = new TestModuleService();
        testModuleDao =  new TestModuleDao();
        sut = new TestModule();
        sut.setName("Java");
        sut.setHardLevel(3);
        sut.setTopic("Beginning");
        sut.setTimeToSolve(15);
    }

    @AfterEach
    void drop() throws DatabaseException, LockException, SQLException {
        h2Connector.dropDb();
    }

    @Test
    void createTest() throws SQLException {
        testModuleService.createTest("test", 5, "test", 5);
        assertEquals("test", testModuleDao.getEntityById(1).getName());
        assertEquals("test", testModuleDao.getEntityById(1).getTopic());
    }

    @Test
    void getTest() throws SQLException {
        testModuleDao.create(sut);
        TestModule testModule = testModuleService.getTest(1);
        assertEquals("Java", testModule.getName());
        assertEquals("Beginning", testModule.getTopic());
    }

    @Test
    void getAllTests() throws SQLException {
        testModuleDao.create(sut);
        List<TestModule> testModules = testModuleService.getAllTests();
        assertEquals(1, testModules.size());
    }

    @Test
    void updateTest() throws SQLException {
        testModuleDao.create(sut);
        testModuleService.updateTest(1, "test", 5, "test", 5);
        assertEquals("test", testModuleDao.getEntityById(1).getName());
        assertEquals("test", testModuleDao.getEntityById(1).getTopic());
    }

    @Test
    void deleteTest() throws SQLException {
        testModuleDao.create(sut);
        testModuleService.deleteTest(1);
        assertEquals(null, testModuleDao.getEntityById(1).getName());
        assertEquals(null, testModuleDao.getEntityById(1).getTopic());
    }

}