package com.epam.projects.pet.dao;


import util.H2Connector;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.exception.LockException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.epam.projects.pet.models.Question;
import com.epam.projects.pet.models.TestModule;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class QuestionDaoTest {
    private H2Connector h2Connector = new H2Connector();
    private QuestionDao questionDao;
    private Question sut;

    @BeforeEach
    void init() throws SQLException, ClassNotFoundException, LiquibaseException {
        h2Connector.buildDb();
        TestModuleDao testModuleDao = new TestModuleDao();
        TestModule testModule = new TestModule();
        testModule.setName("Java");
        testModule.setHardLevel(3);
        testModule.setTopic("Beginning");
        testModule.setTimeToSolve(15);
        testModuleDao.create(testModule);

        questionDao = new QuestionDao();
        sut = new Question();
        sut.setQuestion("Something");
        sut.setTestID(1);
    }

    @AfterEach
    void drop() throws DatabaseException, LockException, SQLException {
        h2Connector.dropDb();
    }

    @Test
    void getAll() throws SQLException {
        assertEquals(0, questionDao.getAll().size());
        questionDao.create(sut);
        assertEquals(1, questionDao.getAll().size());
    }

    @Test
    void update() throws SQLException {
        questionDao.create(sut);
        assertEquals("Something", questionDao.getEntityById(1).getQuestion());
        sut.setQuestionID(1);
        sut.setQuestion("Something else");
        questionDao.update(sut);
        assertEquals("Something else", questionDao.getEntityById(1).getQuestion());
    }

    @Test
    void getEntityById() throws SQLException {
        questionDao.create(sut);
        assertEquals("Something", questionDao.getEntityById(1).getQuestion());
    }

    @Test
    void delete() throws SQLException {
        questionDao.create(sut);
        assertEquals(1, questionDao.getAll().size());
        questionDao.delete(1);
        assertEquals(0, questionDao.getAll().size());
    }

    @Test
    void create() throws SQLException {
        questionDao.create(sut);
        assertEquals("Something", questionDao.getEntityById(1).getQuestion());
        assertEquals(1, questionDao.getEntityById(1).getTestID());
    }

    @Test
    void getQuestionsByTestId() throws SQLException {
        ArrayList<Question> results = questionDao.getQuestionsByTestId(1);
        assertEquals(0, results.size());
    }
}