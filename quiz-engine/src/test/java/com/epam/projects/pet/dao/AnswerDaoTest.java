package com.epam.projects.pet.dao;

import util.H2Connector;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.exception.LockException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.epam.projects.pet.models.Answer;
import com.epam.projects.pet.models.Question;
import com.epam.projects.pet.models.TestModule;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AnswerDaoTest {
    private H2Connector h2Connector = new H2Connector();
    private AnswerDao answerDao;
    private Answer sut;

    @BeforeEach
    void init() throws SQLException, ClassNotFoundException, LiquibaseException {
        h2Connector.buildDb();
        TestModuleDao testModuleDao = new TestModuleDao();
        TestModule testModule = new TestModule();
        testModule.setTestID(1);
        testModule.setName("Java");
        testModule.setHardLevel(3);
        testModule.setTopic("Beginning");
        testModule.setTimeToSolve(15);
        testModuleDao.create(testModule);

        QuestionDao questionDao = new QuestionDao();
        Question question = new Question();
        question.setQuestionID(1);
        question.setQuestion("Something");
        question.setTestID(1);
        questionDao.create(question);

        answerDao = new AnswerDao();
        sut = new Answer();
        sut.setAnswer("Something");
        sut.setCorrect(true);
        sut.setQuestionID(1);
    }

    @AfterEach
    void drop() throws DatabaseException, LockException, SQLException {
        h2Connector.dropDb();
    }

    @Test
    void getAll() throws SQLException {
        assertEquals(0, answerDao.getAll().size());
        answerDao.create(sut);
        assertEquals(1, answerDao.getAll().size());
    }

    @Test
    void update() throws SQLException {
        answerDao.create(sut);
        assertEquals("Something", answerDao.getEntityById(1).getAnswer());
        sut.setAnswerID(1);
        sut.setAnswer("Something else");
        answerDao.update(sut);
        assertEquals("Something else", answerDao.getEntityById(1).getAnswer());
    }

    @Test
    void getEntityById() throws SQLException {
        answerDao.create(sut);
        assertEquals("Something", answerDao.getEntityById(1).getAnswer());
    }

    @Test
    void delete() throws SQLException {
        answerDao.create(sut);
        assertEquals(1, answerDao.getAll().size());
        answerDao.delete(1);
        assertEquals(0, answerDao.getAll().size());
    }

    @Test
    void create() throws SQLException {
        answerDao.create(sut);
        assertEquals("Something", answerDao.getEntityById(1).getAnswer());
        assertEquals(1, answerDao.getEntityById(1).getQuestionID());
    }

    @Test
    void getAnswersByQuestionId() throws SQLException {
        ArrayList<Answer> results = answerDao.getAnswersByQuestionId(1);
        assertEquals(0, results.size());
    }
}