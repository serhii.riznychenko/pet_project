package com.epam.projects.pet.dao;

import util.H2Connector;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.exception.LockException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.epam.projects.pet.models.TestModule;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestModuleDaoTest {
    private H2Connector h2Connector = new H2Connector();
    private TestModuleDao testModuleDao;
    private TestModule sut;

    @BeforeEach
    void init() throws SQLException, ClassNotFoundException, LiquibaseException {
        h2Connector.buildDb();
        testModuleDao =  new TestModuleDao();
        sut = new TestModule();
        sut.setName("Java");
        sut.setHardLevel(3);
        sut.setTopic("Beginning");
        sut.setTimeToSolve(15);
    }

    @AfterEach
    void drop() throws DatabaseException, LockException, SQLException {
        h2Connector.dropDb();
    }

    @Test
    void create() throws SQLException {
        assertEquals(0, testModuleDao.getAll().size());
        //WHEN
        testModuleDao.create(sut);
        //THEN
        assertEquals(1, testModuleDao.getAll().size());
    }

    @Test
    void update() throws SQLException {
        testModuleDao.create(sut);
        assertEquals("Beginning", testModuleDao.getEntityById(1).getTopic());
        sut.setTopic("Something");
        sut.setTestID(1);
        testModuleDao.update(sut);
        assertEquals("Something", testModuleDao.getEntityById(1).getTopic());
    }

    @Test
    void getEntityById() throws SQLException {
        testModuleDao.create(sut);
        assertEquals(15, testModuleDao.getEntityById(1).getTimeToSolve());
    }

    @Test
    void delete() throws SQLException { ;
        testModuleDao.create(sut);
        assertEquals(1, testModuleDao.getAll().size());
        testModuleDao.delete(1);
        assertEquals(0, testModuleDao.getAll().size());
    }
}