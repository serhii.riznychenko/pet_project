package com.epam.projects.pet.dao;

import util.H2Connector;
import com.epam.projects.pet.models.User;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.exception.LockException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class UserDaoTest {
    private H2Connector h2Connector = new H2Connector();
    private UserDao userDao;
    private User sut;

    @BeforeEach
    void init() throws SQLException, ClassNotFoundException, LiquibaseException {
        h2Connector.buildDb();
        userDao = new UserDao();
        sut = new User();
        sut.setLogin("John");
        sut.setEmail("test@i.com");
        char[] pass = {'2', '2', '3', '4', '5'};
        sut.setPassword(pass);
        sut.setRealName("Smith");
        sut.setAge(22);
    }

    @AfterEach
    void drop() throws DatabaseException, LockException, SQLException {
        h2Connector.dropDb();
    }

    @Test
    void incrementingAfterAddingUser() throws SQLException {
        assertEquals(0, userDao.getAll().size());
        //WHEN
        userDao.create(sut);
        //THEN
        assertEquals(1, userDao.getAll().size());
        //WHEN
        sut.setLogin("Stan");
        sut.setEmail("test2@i.com");
        userDao.create(sut);
        //THEN
        assertEquals(2, userDao.getAll().size());
    }

    @Test
    void changingLogin() throws SQLException {
        //WHEN
        userDao.create(sut);
        sut.setUserID(1);
        sut.setLogin("Stan");
        userDao.update(sut);
        //THEN
        Assertions.assertEquals("Stan", userDao.getAll().get(0).getLogin());
    }

    @Test
    void getEntityById() throws SQLException {
        //WHEN
        userDao.create(sut);
        sut.setLogin("Bob");
        sut.setEmail("test1@i.com");
        userDao.create(sut);
        sut.setLogin("Mitch");
        sut.setEmail("test2@i.com");
        userDao.create(sut);
        //THEN
        Assertions.assertEquals("Mitch", userDao.getEntityById(3).getLogin());
    }

    @Test
    void decrementsCountOfUsers() throws SQLException {
        userDao.create(sut);
        //WHEN
        userDao.delete(1);
        //THEN
        assertEquals(0, userDao.getAll().size());
    }

    @Test
    void setsBanOnUser() throws SQLException {
        //WHEN
        userDao.create(sut);
        boolean isBanned = userDao.banUserOp(1, true);
        //THEN
        Assertions.assertEquals(isBanned, userDao.getEntityById(1).getIsBanned());
    }

    @Test
    void passwordsNotSame() throws SQLException {
        userDao.create(sut);
        char[] pass1 = {'t', 'e', 's', 't'};
        //WHEN
        boolean isPassBeenUpdated = userDao.updatePassword(1, pass1);
        //THEN
        assertNotEquals(pass1, userDao.getPassword(1));
        assertEquals(true, isPassBeenUpdated);
    }

    @Test
    void getEntityByLogin() throws SQLException {
        userDao.create(sut);
        User user2 = userDao.getEntityByLogin("John");
        assertEquals("Smith", user2.getRealName());
    }

    @Test
    void russianEncoding() throws SQLException {
        User user = new User();
        user.setLogin("Артем");
        user.setEmail("test@i.com");
        user.setRealName("Артем");
        char[] pass = {'2', '2', '3', '4', '5', '1', '4'};
        userDao.create(user);
        Assertions.assertEquals("Артем", userDao.getEntityById(1).getLogin());
    }
}