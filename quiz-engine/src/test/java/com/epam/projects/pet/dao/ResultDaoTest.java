package com.epam.projects.pet.dao;

import util.H2Connector;
import com.epam.projects.pet.models.TestResult;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.exception.LockException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ResultDaoTest {
    private H2Connector h2Connector = new H2Connector();
    private TestResultDao resultDao;
    private TestResult sut;

    @BeforeEach
    void init() throws SQLException, ClassNotFoundException, LiquibaseException {
        h2Connector.buildDb();
        resultDao = new TestResultDao();
        sut = new TestResult();
        sut.setUserID(1);
        sut.setTestID(1);
        sut.setIsDone(true);
        sut.setPoints(10);
    }

    @AfterEach
    void drop() throws DatabaseException, LockException, SQLException {
        h2Connector.dropDb();
    }

    @Test
    void create() throws SQLException {
        assertEquals(0, resultDao.getAll().size());
        //WHEN
        resultDao.create(sut);
        //THEN
        assertEquals(1, resultDao.getAll().size());
    }

    @Test
    void update() throws SQLException {
        resultDao.create(sut);
        assertEquals(1, resultDao.getEntityById(1).getTestID());
        sut.setPoints(20);
        sut.setResultID(1);
        resultDao.update(sut);
        assertEquals(20, resultDao.getEntityById(1).getPoints());
    }

    @Test
    void getEntityById() throws SQLException {
        resultDao.create(sut);
        assertEquals(10, resultDao.getEntityById(1).getPoints());
    }

    @Test
    void delete() throws SQLException {
        resultDao.create(sut);
        assertEquals(1, resultDao.getAll().size());
        resultDao.delete(1);
        assertEquals(0, resultDao.getAll().size());
    }

    @Test
    void getResultsByUserId() throws SQLException {
        ArrayList<TestResult> results = resultDao.getResultsByUserId(1);
        assertEquals(0, results.size());
    }
}