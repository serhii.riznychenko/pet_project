package com.epam.projects.pet.services;

import com.epam.projects.pet.dao.UserDao;
import com.epam.projects.pet.models.User;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.exception.LockException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import util.H2Connector;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserServiceTest {
    private H2Connector h2Connector = new H2Connector();
    private UserService userService;
    private UserDao userDao;
    private User sut;

    @BeforeEach
    void init() throws SQLException, ClassNotFoundException, LiquibaseException {
        h2Connector.buildDb();
        userService = new UserService();
        userDao = new UserDao();
        sut = new User();
        sut.setUserID(1);
        sut.setLogin("John");
        sut.setEmail("test@i.com");
        char[] pass = {'2', '2', '3', '4', '5'};
        sut.setPassword(pass);
        sut.setRealName("Smith");
        sut.setAge(22);
    }

    @AfterEach
    void drop() throws DatabaseException, LockException, SQLException {
        h2Connector.dropDb();
    }

    @Test
    void getAllUsersExceptAdmins() throws SQLException {
        userDao.create(sut);
        List<User> users = userService.getAllUsersExceptAdmins();
        assertEquals(1, users.size());
    }

    @Test
    void changeUserBanStatusById() throws SQLException {
        userDao.create(sut);
        userService.changeUserBanStatusById(1);
        assertEquals(true, userDao.getEntityById(1).getIsBanned());
    }

    @Test
    void updateUserData() throws SQLException {
        userDao.create(sut);
        userService.updateUserData("test", "test@test.com", "test", 11, sut);
        assertEquals("test", userDao.getEntityById(1).getLogin());
        assertEquals("test", userDao.getEntityById(1).getRealName());
        assertEquals("test@test.com", userDao.getEntityById(1).getEmail());
    }

    @Test
    void deleteUser() throws SQLException {
        userDao.create(sut);
        userService.deleteUser(sut);
        assertEquals(null, userDao.getEntityById(1).getLogin());
        assertEquals(null, userDao.getEntityById(1).getRealName());
    }

    @Test
    void makeAdmin() throws SQLException {
        userDao.create(sut);
        userService.makeAdmin(1, "John");
        assertEquals(User.Role.ADMIN, userDao.getEntityById(1).getRole());
    }
}