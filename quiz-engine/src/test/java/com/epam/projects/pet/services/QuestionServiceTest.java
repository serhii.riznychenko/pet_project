package com.epam.projects.pet.services;

import com.epam.projects.pet.dao.QuestionDao;
import com.epam.projects.pet.dao.TestModuleDao;
import com.epam.projects.pet.models.Question;
import com.epam.projects.pet.models.TestModule;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.exception.LockException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import util.H2Connector;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class QuestionServiceTest {
    private H2Connector h2Connector = new H2Connector();
    private QuestionService questionService;
    private QuestionDao questionDao;
    private Question sut;

    @BeforeEach
    void init() throws SQLException, ClassNotFoundException, LiquibaseException {
        h2Connector.buildDb();
        TestModuleDao testModuleDao = new TestModuleDao();
        TestModule testModule = new TestModule();
        testModule.setName("Java");
        testModule.setHardLevel(3);
        testModule.setTopic("Beginning");
        testModule.setTimeToSolve(15);
        testModuleDao.create(testModule);

        questionService = new QuestionService();
        questionDao = new QuestionDao();
        sut = new Question();
        sut.setQuestion("Something");
        sut.setTestID(1);
    }

    @AfterEach
    void drop() throws DatabaseException, LockException, SQLException {
        h2Connector.dropDb();
    }

    @Test
    void createQuestion() throws SQLException {
        questionService.createQuestion("Something", 1);
        assertEquals("Something", questionDao.getEntityById(1).getQuestion());
    }

    @Test
    void getQuestion() throws SQLException {
        questionDao.create(sut);
        Question question = questionService.getQuestion(1);
        assertEquals("Something", question.getQuestion());
    }

    @Test
    void updateQuestion() throws SQLException {
        questionDao.create(sut);
        questionService.updateQuestion(1, "Test", 1);
        assertEquals("Test", questionDao.getEntityById(1).getQuestion());
    }

    @Test
    void deleteQuestion() throws SQLException {
        questionDao.create(sut);
        questionService.deleteQuestion(1);
        assertEquals(null, questionDao.getEntityById(1).getQuestion());
    }

    @Test
    void loadListOfAnswersIntoListOfQuestions() throws SQLException {
        questionDao.create(sut);
        List<Question> questions = questionService.loadListOfAnswersIntoListOfQuestions(1);
        assertEquals(1, questions.size());
    }
}