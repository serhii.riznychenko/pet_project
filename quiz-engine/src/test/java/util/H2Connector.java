package util;

import com.epam.projects.pet.utils.ConnectionPool;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.exception.LockException;
import liquibase.resource.ClassLoaderResourceAccessor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public  class H2Connector {

    private Liquibase liquibase;
    private Connection connection;

    public H2Connector() {
        ConnectionPool.setPropertyPath("h2.properties");
        connection = ConnectionPool.getInstance().getConnection();


    }

    public synchronized void buildDb() throws SQLException, ClassNotFoundException, LiquibaseException {
     //   Class.forName("org.h2.Driver");
     //   Connection conn = DriverManager.getConnection("jdbc:h2:./testDB/testDB", "sa", "");
     //   Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(conn));
        Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(
                new JdbcConnection(connection));
        liquibase = new Liquibase("liquibase/db-changelog-master.xml",
                new ClassLoaderResourceAccessor(), database);
        liquibase.update("public");
    //    return conn;
        // "src/main/resources/liquibase/db-changelog-master.xml"

    }

    public void dropDb() throws DatabaseException, LockException, SQLException {
        liquibase.dropAll();
        connection.close();
    }
}